#include "stdafx.h"	
#include "Rum.h"

Rum::Rum(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{
	CreateSprite();
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
}
Rum::~Rum()
{
	mAnimationFrames.~AnimateSprite();
}

bool Rum::Update()
{
	mSprite.setPosition(mPosition);
	sf::RectangleShape xCollider;
	xCollider.setPosition(mPosition);
	xCollider.setSize(sf::Vector2f(35.f, 35.f));
	SetColliderRect1(xCollider);
	Move();
	if (mPosition.x + 120 < 0 )
	{
		return false;
	}
	return true;
}

sf::Sprite Rum::GetSprite()
{
	return mSprite;
}
sf::Vector2f Rum::GetPosition()
{
	return mPosition;
}
void Rum::Move()
{
	mPosition += mVelocity;
}
void Rum::CreateSprite()
{
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/level1/rum/Rum.png", 0, 0,120, 120);
	mSprite.setScale(0.5f, 0.5f);
}

EntityType Rum::GetType()
{
	return mType;
}

void Rum::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}
