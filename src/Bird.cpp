#include "Bird.h"

Bird::Bird(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	CreateSprite();
	xSystem.m_pxSoundManager->PlaySound(xSystem.m_pxSoundManager->BirdattackSound, true);
	
	std::random_device generator;
	std::uniform_int_distribution<int> distrubution(1, 7);
	int Position = distrubution(generator);

	mReachEnd = false;
	mPosition.y = -200;
	mPosition.x = (350 * Position);
}

Bird::~Bird()
{
	mAnimationFrames.~AnimateSprite();
}

bool Bird::Update()
{
	mSprite.setPosition(mPosition);
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(50.f, 50.f));
	xCollider.setPosition(sf::Vector2f(mPosition.x + 65, mPosition.y + 75));
	xCollider.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	SetColliderRect1(xCollider);

	Move();
	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));
	if (mPosition.x < 0 - 120)
	{
		return false;
	}
	else
		return true;
}

void Bird::Move()
{
	std::random_device generator;
	std::uniform_int_distribution<int> distrubution(4, 7);
	int Position = distrubution(generator);
	if (mPosition.y > 250*Position)
	{
		mReachEnd = true;
	}

	if (mReachEnd == true)
	{
		mVelocity = sf::Vector2f(-5, -8);
	}
	else
	{
		mVelocity = sf::Vector2f(-5, 8);
	}

	mPosition += mVelocity;
	
	float m_Angle;
	m_Angle = atan2(mVelocity.y,mVelocity.x);

	mSprite.setRotation(m_Angle * (180 / 3.14) + 180);
}

sf::Vector2f Bird::GetPosition()
{
	return mPosition;
}
sf::Sprite Bird::GetSprite()
{
	return mSprite;
}

void Bird::CreateSprite()
{
	mAnimationFrames.AddFramesFromFile(mSystem.m_pxSpriteManager->GetAnimation("../assets/level1/bird/animation/dive.txt"));
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/bird/bird.PNG", 0, 0 ,0,0);
	mSprite.setOrigin(sf::Vector2f(86, 60));
}

EntityType Bird::GetType()
{
	return mType;
}

void Bird::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}

