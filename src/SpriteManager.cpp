#include "SpriteManager.h"
#include <fstream>
#include <sstream>

SpriteManager::SpriteManager()
{
}

SpriteManager::~SpriteManager()
{
	m_axSprites.clear();

	auto it = m_apxTextures.begin();
	while (it != m_apxTextures.end())
	{
	
		delete (it->second);
		(it->second) = nullptr;
		it++;
	}
	m_apxTextures.clear();


	m_apxFrameData.clear();
}


sf::Sprite SpriteManager::CreateSprite(const char* p_sFilepath, float p_iX, float p_iY, int p_iW, int p_iH)
{
	//Check whether texture has been loaded already
	//otherwise load it
	auto it = m_apxTextures.find(p_sFilepath);
	if (it == m_apxTextures.end())
	{
		sf::Texture* xTexture;
		xTexture = new sf::Texture();
		xTexture->loadFromFile(p_sFilepath);


		xTexture->setSmooth(true);
		
		
		xTexture->setRepeated(true);
			m_apxTextures.insert(std::pair<const char*, sf::Texture*>(p_sFilepath, xTexture));
		it = m_apxTextures.find(p_sFilepath);
	}

	//create the sprite
	m_apxTextures.size();
	sf::Sprite xSprite;
	xSprite.setTexture(*it->second);
	xSprite.setTextureRect(sf::IntRect(p_iX, p_iY, p_iW, p_iH));
	m_axSprites.push_back(xSprite);
	return xSprite;
}

sf::FloatRect SpriteManager::SetTextureRect(sf::Sprite spriteref)
{
	sf::FloatRect xRect;
	xRect = spriteref.getLocalBounds();
	return xRect;
}

std::vector<FrameData> SpriteManager::GetAnimation(const char* p_sFilePath)
{
	m_axFrames.clear();

	auto it = m_apxFrameData.find(p_sFilePath);
	if (it == m_apxFrameData.end())
	{
	
		float number[6];
		int i = 0;
		std::string line;
		std::ifstream doc;
		doc.open(p_sFilePath);
		while (doc.good())
		{
			
			std::getline(doc, line);
			std::stringstream convert(line);

			convert >> number[i];

			i++;
			if (i == 6)
			{
				Myframes.m_xRegion.left = number[0];
				Myframes.m_xRegion.top = number[1];
				Myframes.m_xRegion.width = number[2];
				Myframes.m_xRegion.height = number[3];
				Myframes.m_fDuration = number[4];
				m_axFrames.push_back(Myframes);
				i = 0;
			}
		}
		doc.close();
		m_apxFrameData.insert(std::pair<const char*, std::vector<FrameData>>(p_sFilePath, m_axFrames));
		it = m_apxFrameData.find(p_sFilePath);
	}


	return it->second;
}