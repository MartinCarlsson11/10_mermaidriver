#include "Collider.h"

Collider::Collider()
{

}

Collider::Collider(sf::CircleShape xColliderCircle)
{

}

Collider::Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
	sf::RectangleShape xColliderRect3, sf::RectangleShape xColliderRect4)
{

}

Collider::Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
	sf::RectangleShape xColliderRect3)
{

}

Collider::Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2)
{

}

Collider::Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1)
{

}

Collider::Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
	sf::RectangleShape xColliderRect3, sf::RectangleShape xColliderRect4)
{

}

Collider::Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
	sf::RectangleShape xColliderRect3)
{

}

Collider::Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2)
{

}

Collider::Collider(sf::RectangleShape xColliderRect1)
{

}

Collider::Collider::~Collider()
{
}


sf::RectangleShape Collider::GetColliderRect1()
{
	return m_xColliderRect1;
}
sf::RectangleShape Collider::GetColliderRect2()
{
	return m_xColliderRect2;
}
sf::RectangleShape Collider::GetColliderRect3()
{
	return m_xColliderRect3;
}
sf::RectangleShape Collider::GetColliderRect4()
{
	return m_xColliderRect4;
}
sf::CircleShape Collider::GetColliderCircle1()
{
	return m_xColliderCircle1;
}

void Collider::SetColliderRect1(sf::RectangleShape xRect)
{
	m_xColliderRect1 = xRect;
}
void Collider::SetColliderRect2(sf::RectangleShape xRect)
{
	m_xColliderRect2 = xRect;
}
void Collider::SetColliderRect3(sf::RectangleShape xRect)
{
	m_xColliderRect3 = xRect;
}
void Collider::SetColliderRect4(sf::RectangleShape xRect)
{
	m_xColliderRect4 = xRect;
}
void Collider::SetColliderCircle1(sf::CircleShape xCircle)
{
	m_xColliderCircle1 = xCircle;
}