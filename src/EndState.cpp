#include "EndState.h"
#include <iostream>
#include "MenuState.h"
#include <fstream>
EndState::EndState(System& p_xSystem, bool xWin, HighscoreManager xHighscore)
{
	m_xSystem = p_xSystem;
	WinOrLose = xWin;
	Highscore = std::to_string(xHighscore.GetHighscore());
	m_xSystem.m_pxWindow->setMouseCursorVisible(true);
}

EndState::~EndState()
{

	doc.open("../assets/highscore.txt");
	while (std::getline(doc, line))
	{
		Entries.push_back(line);
	}
	for (int i = 0; i < 4; i++)
	{
		if (Entries[i].end() == std::find_if(Entries[i].begin(), Entries[i].end(),
			[](unsigned char c)->bool { return !isdigit(c); })) {
			if (stoi(Highscore) > stoi(Entries[i]))
			{
				if (i + 1 < 4)
				{
					if (i + 1 + 1 < 4)
					{
						if (i + 1 + 1 + 1 < 4)
						{
							Entries[i + 1 + 1 + 1] = Entries[i + 1 + 1];
							Entries[i + 1 + 1 + 1 + 4] = Entries[i + 1 + 1 +4];
						}
						Entries[i + 1 + 1] = Entries[i + 1];
						Entries[i + 1 + 1 +4] = Entries[i + 1 + 4];
					}
					Entries[i + 1] = Entries[i];
					Entries[i + 1 +4] = Entries[i + 4];
				}
				Entries[i] = Highscore;
				Entries[i + 4] = Name;
				i = Entries.size();
			}
		}
	}
	doc.close();

	m_xSystem.m_pxSoundManager->StopMusic();
	doc.open("../assets/highscore.txt");
	for (int i = 0; i < Entries.size(); i++)
	{
		doc << Entries[i] << std::endl;
	}
	doc.close();
}

void EndState::Enter()
{
	
	if (WinOrLose == false)
	{
		m_xSystem.m_pxSoundManager->PlayMusic(m_xSystem.m_pxSoundManager->DeathMusic);
		m_xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Menu/gameover.png", 0, 0, 1920, 1080);
	}
	else
	{
		m_xSystem.m_pxSoundManager->PlayMusic(m_xSystem.m_pxSoundManager->WinMusic);
		m_xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Menu/winscreen.png", 0, 0, 1920, 1080);
	}

	font.loadFromFile("../assets/font/Lato-Regular.ttf");
	if (WinOrLose == false)
	{
		Entry1.setPosition(sf::Vector2f(1500, 280));
		Entry2.setPosition(1300, 610);

		mMenu = Menu(m_xSystem, Button_Continue, sf::FloatRect(1200, 850, 0, 0));
	}
	else
	{
		Entry2.setPosition(sf::Vector2f(835, 760));
		Entry1.setPosition(1380, 430);
		
		mMenu = Menu(m_xSystem, Button_Continue, sf::FloatRect(m_xSystem.m_iScreenWidth / 2, m_xSystem.m_iScreenHeight - 200, 0, 0));
	}
	Entry1.setString(Highscore);
	Entry1.setFont(font);
	Entry1.setOrigin(Entry1.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);

	
	Entry2.setFont(font);	
	Entry2.setOrigin(Entry1.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);
	m_xSystem.m_xEngine->ResetTextEntered();
	Name = " "; // get input;


}

void EndState::Draw()
{
	m_xSystem.m_pxDrawManager->SetDefaultView();
	m_xSystem.m_pxDrawManager->Draw(m_xSprite, sf::Vector2f(0, 0));

	m_xSystem.m_pxDrawManager->Draw(Entry1);
	m_xSystem.m_pxDrawManager->Draw(Entry2);
	m_xSystem.m_pxDrawManager->Draw(mMenu);
}

bool EndState::Update(float p_fDeltaTime)
{
	Name = m_xSystem.m_xEngine->GetTextEntered();

	Entry2.setString("Captain " + Name);


	if (!mMenu.Update())
	{
		for (int i = 0; i < Entries.size(); i++)
		{
			doc << Entries[i];
		}
		return false;
	}
	return true;
}


void EndState::Exit()
{

}

IState* EndState::NextState()
{
	return new MenuState(m_xSystem);
}

