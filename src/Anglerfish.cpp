#include "stdafx.h"

Anglerfish::Anglerfish(sf::Vector2f p_position, System xSystem)
{
	m_xPosition = p_position;
	CreateSprite(xSystem);
}

Anglerfish::~Anglerfish()
{

}

bool Anglerfish::Update()
{
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(40.f, 35.f));
	xCollider.setPosition(sf::Vector2f(m_xPosition.x, m_xPosition.y + 40.f));

	SetColliderRect1(xCollider);

	xAccumulator += limitMovement.restart();
	//tuning options, frequency of keypresses,
	//Velocity amount set from each press. 
	//Deacceleration amount. 


	std::random_device generator;
	std::uniform_int_distribution<int> distrubution(1, 3);
	int RandomNumber = distrubution(generator);

	//bool if enemy detected or not
	if (xAccumulator.asSeconds() > 3.0f)
	{
		if (RandomNumber == 1)
		{
			if (m_xPosition.x < 0)
			{
				m_xVelocity.x = +1.0;
				m_xSprite.setRotation(180);
				m_xSprite.setScale(1.0f, -1.0f);
			}
			else if (m_xPosition.x > 1920)
			{
				m_xVelocity.x = -1.0;
				m_xSprite.setRotation(0);
				m_xSprite.setScale(1.0f, 1.0f);
			}
			limitMovement.restart();
			xAccumulator = sf::Time::Zero;
		}
		else if (RandomNumber == 2)
		{
			m_xVelocity.y = 1.0f;
			limitMovement.restart();
			xAccumulator = sf::Time::Zero;
		}

		else
		{
			m_xVelocity.y = -1.0f;
			limitMovement.restart();
			xAccumulator = sf::Time::Zero;
		}
	}
	
	if (m_xPosition.y >= 800)
	{
		m_xPosition.y = 800;
	}
	if (m_xPosition.y <= 0)
	{
		m_xPosition.y = 0;
	}

	m_xSprite.setTextureRect(m_xAnimationFrames.UpdateASprite(0.16));

	Move();
	if (m_xPosition.x < 0 + 120)
	{
		return false;
	}
	else
		return true;
}

void Anglerfish::Move()
{
	m_xPosition.x += m_xVelocity.x;
	m_xPosition.y += m_xVelocity.y;
}

sf::Vector2f Anglerfish::GetPosition()
{
	return m_xPosition;
}
sf::Sprite Anglerfish::GetSprite()
{
	return m_xSprite;
}

sf::Vector2f Anglerfish::GetVelocity()
{
	return m_xVelocity;
}

void Anglerfish::CreateSprite(System x_System)
{
	m_xSprite = x_System.m_pxSpriteManager->CreateSprite("../assets/Level1/anglerfish/anglerfish.PNG", 0, 0, 0,0);

	m_xAnimationFrames.AddFramesFromFile(x_System.m_pxSpriteManager->GetAnimation("../assets/level1/anglerfish/animation/idle.txt"));
}