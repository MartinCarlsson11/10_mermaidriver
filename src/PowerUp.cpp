#include "stdafx.h"
#include "ParticleHandler.h"
#include "PowerUp.h"

PowerUp::PowerUp(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	CreateSprite();
	mParticleHandler = new ParticleHandler(mSystem, 10, mPosition, sf::Color::Transparent);
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
	Timer.restart();
}
PowerUp::~PowerUp()
{
	delete mParticleHandler;
	mParticleHandler = nullptr;
	mAnimationFrames.~AnimateSprite();
}
bool PowerUp::Update()
{
	mSprite.setPosition(mPosition);
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(40, 40));
	xCollider.setPosition(sf::Vector2f(mPosition.x + 5, mPosition.y));
	
	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16));
	SetColliderRect1(xCollider);
	Accu += Timer.restart();

	//set timer to random
	if (mPosition.x < 1500)
	{
		mVelocity = sf::Vector2f(-mScrollSpeed, -2.f);
	}
		
	mParticleHandler->Update(0.016, mPosition, 30-Accu.asSeconds());

	if (mPosition.y < 720)
	{
		return false;
	}
	Move();
	return true;
}

ParticleHandler* PowerUp::getParticles()
{
	return mParticleHandler;
}
sf::Sprite PowerUp::GetSprite()
{
	return mSprite;
}
sf::Vector2f PowerUp::GetPosition()
{
	return mPosition;
}

EntityType PowerUp::GetType()
{
	return mType;
}

void PowerUp::CreateSprite()
{
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/powerups/bubblepowerup.png",0 , 0, 0, 0);
	
	mAnimationFrames.AddFrame(0,0, 49, 47, 0.05);
	mAnimationFrames.AddFrame(49, 0, 49, 47,0.05);
	mAnimationFrames.AddFrame(98, 0, 49, 47,0.05);
	mAnimationFrames.AddFrame(147, 0,  49, 47,0.05);
	mAnimationFrames.AddFrame(196, 0,  49, 47,0.05);
	mAnimationFrames.AddFrame(245, 0,49, 47, 5);
}
void PowerUp::Move()
{
	mPosition += mVelocity;
}


void PowerUp::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
	target.draw(*mParticleHandler);
}
