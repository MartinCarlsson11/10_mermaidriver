#include "stdafx.h"
#include "Swordfish.h"
#include "Captain.h"
#include <memory>

Swordfish::Swordfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed,System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{
	mVelocity = sf::Vector2f(-mScrollSpeed * 3, 0);
	CreateSprite();
	mSprite = mSpriteIdle;
	mAnimationFrames = mAnimationFrames_Idle;
	mSetLoopAnimationTrue = false;
}

Swordfish::~Swordfish()
{
	mAnimationFrames.~AnimateSprite();
}

bool Swordfish::Update()
{
	sf::RectangleShape xCollider1;
	xCollider1.setPosition(sf::Vector2f(mPosition.x, mPosition.y + 100));
	xCollider1.setSize(sf::Vector2f(200, 60));
	SetColliderRect1(xCollider1);

	mTargetPosition = Captain::GetCaptainPosition();
	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));

	if (mPosition.x + 400 < 1920)
	{
		if (xCollider1.getPosition().y+25 >= mTargetPosition.y && mFindYPositive == false)
		{
			mVelocity.y = -1.5f;
			mVelocity.x = 0.f;
			mFindYPositive = true;
			if (mFindYNegative == true)
			{
				mChargeClock.restart();
			}
		}
		else if (xCollider1.getPosition().y+25 <= mTargetPosition.y && mFindYNegative == false)
		{
			mVelocity.y = 1.5f;
			mVelocity.x = 0.f;
			mFindYNegative = true;
			if (mFindYPositive == true)
			{
				mChargeClock.restart();
			}
		}

		else if (mFindYPositive == true && mFindYNegative == true)
		{
			
			mVelocity.y = 0.f;
			if (mSetLoopAnimationTrue == false)
			{
				mSprite = mSpriteCharge;
				mSprite.setPosition(mPosition);
				mAnimationFrames = mAnimationFrames_Loop;
				mSetLoopAnimationTrue = true;
			}

			mChargeAccumulator += mChargeClock.getElapsedTime();
			if (mChargeAccumulator.asSeconds() > 3 )
			{
				mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->SwordfishLungeSound, true);
				mSprite.setTextureRect(sf::IntRect(2100, 0, 350, 233));
				mVelocity.x = -20.0f;
			}
		}


	}
	

	FindCaptain();
	Move();

	mSprite.setPosition(mPosition);


	if (mPosition.x + mSprite.getLocalBounds().width < 0)
	{
		return false;
	}
	return true;
}
void Swordfish::Move()
{
	mPosition += mVelocity;
}

sf::Sprite Swordfish::GetSprite()
{
	return mSprite;
}

sf::Vector2f Swordfish::GetPosition()
{
	return mPosition;
}

void Swordfish::CreateSprite()
{
	mAnimationFrames_Loop.AddFrame(0, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(350, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(700, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(1050, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(1400, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(1750, 0, 350, 233, 0.7);
	mAnimationFrames_Loop.AddFrame(2100, 0, 350, 233, 0.7);

	mAnimationFrames_Charge.AddFrame(2100, 0, 350, 233, 10);

	mAnimationFrames_Idle.AddFrame(0, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(350, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(700, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(1050, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(1400, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(1750, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(2100, 0, 350, 233, 1);
	mAnimationFrames_Idle.AddFrame(2450, 0, 350, 233, 1);

	mSpriteIdle = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/Swordfish/swordfish idle.PNG", 0, 0, 0, 0);

	mSpriteCharge = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/Swordfish/swordfish charge.PNG", 0, 0, 0, 0);
}


EntityType Swordfish::GetType()
{
	return mType;
}

void Swordfish::FindCaptain()
{ 


	
}


void Swordfish::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
};
