#include "stdafx.h"
#include "LevelLoadState.h"
#include "GameState.h"

LevelLoad::LevelLoad(System& xSystem)
{
	//m_xParallaxHandler = nullptr;
	m_xSystem = xSystem;
	//font.loadFromFile("../assets/font/Lato-Regular.ttf");
//	text.setFont(font);
	text.setPosition(m_xSystem.m_iScreenWidth / 2, m_xSystem.m_iScreenHeight / 2);
	text.setOrigin(text.getGlobalBounds().width / 2, text.getGlobalBounds().height / 2);
	text.setString("Loading Level: Please Wait!");
}

LevelLoad::~LevelLoad()
{
}

bool LevelLoad::LoadTheLevel()
{
//	if (m_xParallaxHandler == nullptr)
	{
		//m_xParallaxHandler = new ParallaxHandler();
	}
//	if (m_xLevel == nullptr)
	{
		//m_xLevel = new Level(m_xSystem.m_pxSpriteManager);
	}
	
	text.setString("Loading Complete!");
	
	return true;
}

void LevelLoad::Enter()
{

}
bool LevelLoad::Update(float p_fDeltaTime)
{
	//if (LoadTheLevel())
	//{
	return false;
}
void LevelLoad::Draw()
{
	//m_xSystem.m_pxDrawManager->Draw(text);
}
void LevelLoad::Exit()
{
}
IState* LevelLoad::NextState()
{
	return new GameState(m_xSystem);
}