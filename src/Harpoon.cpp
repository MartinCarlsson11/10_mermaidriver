#include "stdafx.h"
#include <math.h>
#include <cmath>
#include "Harpoon.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "Captain.h"

Harpoon::Harpoon(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{

	CreateSprite();

	m_bReturnHarpoon = false;

	m_xDirection = sf::Vector2f(0.f, 0.f);
	mVelocity = sf::Vector2f(0.0f, 0.0f);
	SimpleTimerClock.restart();
	m_pxThrowTimer = nullptr;
	mSprite.setOrigin(5, mSprite.getGlobalBounds().height / 2);
	

	
	m_xHarpoonState = false;
	m_bIsVisible = false;
	PossessHarpoon = true;
}

Harpoon::~Harpoon()
{
	mAnimationFrames.~AnimateSprite();
}

bool Harpoon::Update(sf::View xView, Captain xCaptain, bool ElectricityDamage)
{
	mHarpoonStateStatic = PossessHarpoon;
	if (HarpoonState())
	{
		xCollider2.setSize(sf::Vector2f(50, 15));
	}
	else
	{
		xCollider2.setSize(sf::Vector2f(0, 0));
	}
	

	if (PossessHarpoon)
	{
		xCollider1.setSize(sf::Vector2f(0, 0));
		xCollider1.setPosition(sf::Vector2f(mPosition.x, mPosition.y));
	}
	else
	{
		xCollider1.setOrigin(0, -2);
		xCollider2.setOrigin(-175, 2);
		xCollider1.setSize(sf::Vector2f(160, 4));
		xCollider1.setPosition(sf::Vector2f(mPosition.x, mPosition.y));
		xCollider2.setPosition(sf::Vector2f(mPosition.x, mPosition.y));
	}

	
	if (m_pxThrowTimer != nullptr)
	{
		m_pxThrowTimer->Update(SimpleTimerClock.getElapsedTime());
	}




	if ((!sf::Mouse::isButtonPressed(sf::Mouse::Left)) && m_pxThrowTimer != nullptr && (PossessHarpoon == true))
	{
		m_bReturnHarpoon = false;
		xShoot = false;
		TimeKeeper = 0;
		fullWindup = false;
		mVelocity = sf::Vector2f(0.0f, 0.0f);
		mPosition = xCaptain.GetPosition();
		m_xDeceleration = sf::Vector2f(0.0f, 0.0f);
		float m_Angle = 0;

		sf::Vector2i Mouse = sf::Mouse::getPosition();
		sf::Vector2f WorldMouse = mSystem.m_pxWindow->mapPixelToCoords(Mouse, xView);

		if (mPosition.x - sf::Mouse::getPosition(*mSystem.m_pxWindow).x < 0)
		{
			if (mPosition.y - sf::Mouse::getPosition(*mSystem.m_pxWindow).y > 0)
			{
				m_Angle = atan2(mPosition.y - WorldMouse.y , mPosition.x  - WorldMouse.x);
			}
			else 
			{
				m_Angle = atan2(mPosition.y - WorldMouse.y, mPosition.x - WorldMouse.x);
			}
		}
		else
		{
			if (mPosition.y - sf::Mouse::getPosition().y > 0)
			{
				m_Angle = atan2(mPosition.y - WorldMouse.y, mPosition.x - WorldMouse.x);
			}
			else
			{
				m_Angle = atan2(mPosition.y - WorldMouse.y, mPosition.x - WorldMouse.x);
			}
		}
		float xAngle = cos(m_Angle);
		float yAngle = sin(m_Angle);

		Velocity = sf::Vector2f(-xAngle, -yAngle);

		mSprite.setRotation(m_Angle * 180 / 3.14f + 180);
		xCollider1.setRotation(m_Angle * 180 / 3.14f + 180);
		xCollider2.setRotation(m_Angle * 180 / 3.14f + 180);
		m_xRope.setRotation(m_Angle * 180 / 3.14f + 180);

		if (m_pxThrowTimer->Check() == 1)
		{
			fullWindup = true;
		}
		xShoot = true;

		TimeKeeper = m_pxThrowTimer->GetTime();
		delete m_pxThrowTimer;
		m_pxThrowTimer = nullptr;
		m_xHarpoonState = true;
		m_bIsVisible = true;
		PossessHarpoon = false;
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && m_xHarpoonState == false)
	{
		m_bReturnHarpoon = true;
	}

	if ((sf::Mouse::isButtonPressed(sf::Mouse::Left)) && (m_pxThrowTimer == nullptr) && (PossessHarpoon == true))
	{
		m_pxThrowTimer = new SimpleTimer(2000, SimpleTimerClock.getElapsedTime());
	}

	if (mSprite.getRotation() > 270 || mSprite.getRotation() < 90)
	{
		mSprite.setScale(0.7f, 0.7f);
		xCollider1.setScale(1.0f, 1.0f);
		xCollider2.setScale(1.0f, 1.0f);
	}
	else
	{
		mSprite.setScale(0.7f, -0.7f);
		xCollider1.setScale(1.0f, -1.0f);
		xCollider2.setScale(1.0f, -1.0f);
	}

	if (m_bReturnHarpoon != true && m_xHarpoonState == true)
	{
		Deacceleration();
		Shoot(xCaptain, Velocity);
	}
	if ((m_bReturnHarpoon == true) && m_xHarpoonState != true)
	{
		ResetHarpoon(xCaptain.GetPosition());
	}

	SetColliderRect1(xCollider1);
	SetColliderRect2(xCollider2);
	SetColliderCircle1(xColliderCircle);
	UpdateRope(xCaptain, ElectricityDamage);
	Move();
	return true;
}

bool Harpoon::Update()
{
	return true;
}

sf::Sprite Harpoon::GetSprite()
{
	return mSprite;
}

void Harpoon::Move()
{
	if (mVelocity.x <  0.1f && mVelocity.x > -0.1f)
	{
		mVelocity.x = 0;
	}
	if (mVelocity.y < 0.1f && mVelocity.y > -0.1f)
	{
		mVelocity.y = 0;
	}
	if (mVelocity.x == 0 && mVelocity.y == 0)
	{
		m_xHarpoonState = false;
	}
	mPosition += mVelocity;
	mSprite.setPosition(mPosition);

	if (mPosition.x - 100 < 0)
	{
		mPosition.x = 100;
		mVelocity = sf::Vector2f(0, 0);
		m_xHarpoonState = false;
	}
	if (mPosition.y - 120 < 0)
	{
		mVelocity = sf::Vector2f(0, 0);
		m_xHarpoonState = false;
	}
	if (mPosition.x + 150 > 1920)
	{
		mPosition.x = 1770;
		mVelocity = sf::Vector2f(0, 0);
		m_xHarpoonState = false;
	}
	if (mPosition.y + 120 > 1720)
	{
		mVelocity = sf::Vector2f(0, 0);
		m_xHarpoonState = false;
	}

}
	
void Harpoon::Deacceleration()
{
	
	if (mVelocity.x > 0)
	{
		m_xDeceleration.x += 0.15f;
	}

	if (mVelocity.x < 0)
	{
		m_xDeceleration.x += 0.15f;
	}
	
	if (mVelocity.y < 0)
	{
		m_xDeceleration.y += 0.15f;
	}
	if (mVelocity.y > 0)
	{
		m_xDeceleration.y += 0.15f;
	}
	
}

sf::Vector2f Harpoon::GetPosition()
{
	return mPosition;
}

sf::Vector2f Harpoon::GetVelocity()
{
	return mVelocity;
}

void Harpoon::CreateSprite()
{
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Captain/harpoon.png", 0, 0, 323, 46);
	mSprite.setScale(0.7f, 0.7f);

	m_xRopeSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Captain/rope.png", 0, 0, 0, 0);
}

void Harpoon::Shoot(Captain xCaptain, sf::Vector2f xVelocity)
{
	if (fullWindup)
	{
		xVelocity.x *= 30- m_xDeceleration.x;
		xVelocity.y *= (30- m_xDeceleration.y);
	}
	else
	{
		xVelocity.x *= ((TimeKeeper / 75))- m_xDeceleration.x;
		xVelocity.y *= ((TimeKeeper / 75))- m_xDeceleration.y;
	}

	mVelocity = xVelocity;
}

void Harpoon::ResetHarpoon(sf::Vector2f xTargetPos)
{
	m_xAngle = atan2(xTargetPos.y - mPosition.y, xTargetPos.x - mPosition.x);

	mVelocity.x = cos(m_xAngle) * 10.f;
	mVelocity.y = sin(m_xAngle) * 10.f;
}


bool Harpoon::HarpoonState()
{
	return m_xHarpoonState;
}

void Harpoon::HarpoonInactive()
{
	m_xDeceleration = sf::Vector2f(0.f, 0.f);
	mVelocity = sf::Vector2f(0.f, 0.f);
	m_bReturnHarpoon = false;
	m_xHarpoonState = false;
	m_bIsVisible = false;
	PossessHarpoon = true;
}


bool Harpoon::IsVisible()
{
	return m_bIsVisible;
}

float Harpoon::GetWindupTime()
{
	if (m_pxThrowTimer != nullptr)
	{
		if (m_pxThrowTimer->GetTime() < 2000)
		{
			return m_pxThrowTimer->GetTime() / 100.f;
		}
		else return 20.f;
	}
	else return 0.0f;
}


sf::RectangleShape Harpoon::HarpoonRope()
{
	m_xRope.setTexture(m_xRopeSprite.getTexture());
	
	return m_xRope;
}

void Harpoon::UpdateRope(Captain xCaptain, bool xToggleElectricity)
{
	float RopeSize = 0;
	m_xRope.setPosition(xCaptain.GetPosition());

	float aSquare = (mPosition.y - xCaptain.GetPosition().y) * (mPosition.y - xCaptain.GetPosition().y);
	float bSquare = (mPosition.x - xCaptain.GetPosition().x) *(mPosition.x - xCaptain.GetPosition().x);

	RopeSize = std::sqrt(aSquare + bSquare);

	if (xToggleElectricity == true)
	{
		m_xRope.setTextureRect(sf::IntRect(0, 50, RopeSize, 50));
		m_pxElectricClock.restart();
	}

	ElectricAccu = m_pxElectricClock.getElapsedTime();
	if (ElectricAccu.asSeconds() > 2.f)
	{
		m_xRope.setTextureRect(sf::IntRect(0, 0, RopeSize, 50));

	}

	
	m_xRope.setSize(sf::Vector2f(RopeSize, 25));
	
	if (xCaptain.GetPosition().x - mPosition.x < 0)
	{
		m_xRope.setRotation(atan2(xCaptain.GetPosition().y - mPosition.y, xCaptain.GetPosition().x - mPosition.x) * 180 / 3.14f + 180);
	}
	else
	{
		m_xRope.setRotation(atan2(xCaptain.GetPosition().y - mPosition.y, xCaptain.GetPosition().x - mPosition.x) * 180 / 3.14f + 180);
	}

}


EntityType Harpoon::GetType()
{
	return mType;
}

void Harpoon::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}

bool Harpoon::mHarpoonStateStatic = false;

bool Harpoon::GetHarpoonState()
{
	return mHarpoonStateStatic;
}