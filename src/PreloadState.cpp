#include "stdafx.h"

#include "PreloadState.h"
#include "MenuState.h"

Preload::Preload(System& xSystem)
{
	m_xSystem = xSystem;

	TexturePaths.push_back("../assets/level1/projectile.png");
	TexturePaths.push_back("../assets/Level1/powerups/bubblepowerup.png");
	TexturePaths.push_back("../assets/Level1/jellyfish/Jellyfish.png");
	TexturePaths.push_back("../assets/Captain/harpoon.png");
	TexturePaths.push_back("../assets/Captain/rope.png");
	TexturePaths.push_back("../assets/Level1/flyfish/flyfish.PNG");
	TexturePaths.push_back("../Assets/level1/death/Swordfish.png");
	TexturePaths.push_back("../Assets/level1/death/Jellyfish.png");
	TexturePaths.push_back("../Assets/level1/death/Blowfish.png");
	TexturePaths.push_back("../assets/Level1/coins/coin.png");

	TexturePaths.push_back("../assets/Level1/blowfish/blowfish.png");
	TexturePaths.push_back("../assets/Level1/bird/bird.PNG");
	TexturePaths.push_back("../assets/Level1/anglerfish/anglerfish.PNG");
	TexturePaths.push_back("../assets/Level1/Background/Tileset.png");
	TexturePaths.push_back("../assets/Menu/splash.png");
	TexturePaths.push_back("../assets/Menu/buttons.png");
	TexturePaths.push_back("../assets/Menu/cursor.png");
	TexturePaths.push_back("../assets/Menu/Gameover.png");
	TexturePaths.push_back("../assets/Menu/highscore.png");
	TexturePaths.push_back("../assets/Menu/winscreen.png");

	TexturePaths.push_back("../assets/gui/Damagefeedback.png");
	TexturePaths.push_back("../assets/gui/drownfeedback.png");

	TexturePaths.push_back("../assets/level1/surface.png");
	TexturePaths.push_back("../assets/Gui/hud.png");
	TexturePaths.push_back("../assets/menu/cursor.png");
	TexturePaths.push_back("../assets/level1/chest.png");
	TexturePaths.push_back("../assets/level1/controls.png");
	
	TexturePaths.push_back("../assets/captain/cpt_electric.PNG");
	TexturePaths.push_back("../assets/captain/Cpt_hurt.png");
	TexturePaths.push_back("../assets/captain/cpt_idle arm empty.PNG");
	TexturePaths.push_back("../assets/captain/cpt_idle arm reel.PNG");
	TexturePaths.push_back("../assets/captain/cpt_idle arm.PNG");
	TexturePaths.push_back("../assets/captain/cpt_idle.PNG");
	TexturePaths.push_back("../assets/captain/cpt_swim forward.PNG");
	TexturePaths.push_back("../assets/captain/cpt_swim_forward arm empty.PNG");
	TexturePaths.push_back("../assets/captain/cpt_swim_forward arm reel.PNG");
	TexturePaths.push_back("../assets/captain/cpt_swim_forward arm.PNG");
	TexturePaths.push_back("../assets/captain/cpt_throw.PNG");



	AnimationPaths.push_back("../assets/level1/Swordfish/animation/idle.txt");
	AnimationPaths.push_back("../assets/level1/Swordfish/animation/Charge Forward.txt");
	AnimationPaths.push_back("../assets/level1/Swordfish/animation/Charge Up.txt");
	AnimationPaths.push_back("../assets/level1/jellyfish/animation/idle.txt");
	AnimationPaths.push_back("../assets/level1/flyfish/animation/idle.txt");
	AnimationPaths.push_back("../assets/captain/animation frames/idle.txt");
	AnimationPaths.push_back("../assets/level1/blowfish/animation/idle.txt");
	AnimationPaths.push_back("../assets/level1/bird/animation/dive.txt");
	AnimationPaths.push_back("../assets/level1/anglerfish/animation/idle.txt");

	font.loadFromFile("../assets/font/Lato-Regular.ttf");


	Progress = 0;
	LoadTextures = 0;
	LoadAnimations = 0;

	Finished = AnimationPaths.size() + TexturePaths.size();

	
	text.setFont(font);
	text.setPosition(m_xSystem.m_iScreenWidth / 2, m_xSystem.m_iScreenHeight / 2);
	text.setOrigin(text.getGlobalBounds().width / 2, text.getGlobalBounds().height / 2);
}

Preload::~Preload()
{
	TexturePaths.clear();
	AnimationPaths.clear();
	
}
void Preload::Enter()
{
	
}
bool Preload::Update(float p_fDeltaTime)
{
	text.setString("Loading: " + std::to_string((int)((Progress / Finished) * 100.f)) + "%");
	if (TexturePaths.size() == LoadTextures+1)
	{

		if (AnimationPaths.size() == LoadAnimations +1)
		{
			return false;
		}
		else
		{
			m_xSystem.m_pxSpriteManager->GetAnimation(AnimationPaths[LoadAnimations]);
			m_xSystem.m_pxDrawManager->Clear();
			m_xSystem.m_pxDrawManager->Draw(text);
			m_xSystem.m_pxDrawManager->Display();
			LoadAnimations++;
			Progress++;
			
			return true;
		}
	}
	else
	{
		m_xSystem.m_pxSpriteManager->CreateSprite(TexturePaths[LoadTextures], 0, 0, 0, 0);
		m_xSystem.m_pxDrawManager->Clear();
		m_xSystem.m_pxDrawManager->Draw(text);
		m_xSystem.m_pxDrawManager->Display();
		LoadTextures++;
		Progress++;
		return true;
	}
}
void Preload::Draw()
{
	
}
void Preload::Exit()
{

}
IState* Preload::NextState()
{
	return new MenuState(m_xSystem);
}