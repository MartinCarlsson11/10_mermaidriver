#include "stdafx.h"
#include "SimpleTimer.h"

SimpleTimer::SimpleTimer(int p_iInterval, sf::Time CurrentTime)
{
	m_xInterval = p_iInterval;
	m_xAccumulator = 0;
	m_xTotal = 0;
	m_xCurrentTime = CurrentTime.asMilliseconds();
	m_xLastTime = m_xCurrentTime;
}
SimpleTimer::~SimpleTimer()
{

}

int SimpleTimer::Ready()
{
	return m_xAccumulator > m_xInterval;
}
int SimpleTimer::Check()
{
	if (Ready())
	{
		m_xAccumulator -= m_xInterval;
		return 1;
	}
	return 0;
}
void SimpleTimer::Update(sf::Time CurrentTime)
{
	int m_xDelta = 0;
	m_xCurrentTime = CurrentTime.asMilliseconds();
	m_xDelta = m_xCurrentTime - m_xLastTime;
	m_xTotal += m_xDelta;
	m_xAccumulator += m_xDelta;
	m_xLastTime = m_xCurrentTime;
}


int SimpleTimer::GetTime()
{
	return m_xAccumulator;
}