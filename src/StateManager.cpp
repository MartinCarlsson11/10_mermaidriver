#include "StateManager.h"
#include "IState.h"

StateManager::StateManager()
{
	m_pxCurrentState = nullptr;

}


StateManager::~StateManager()
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
}


bool StateManager::Update()
{
	if (m_pxCurrentState != nullptr)
	{
		if (m_pxCurrentState->Update(0) == false)
		{
			SetState(m_pxCurrentState->NextState());
		}
	}
	else
	{
		return false;
	}
	return true;
}


void StateManager::Draw()
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Draw();
	}
}

void StateManager::SetState(IState* p_pxState)
{
	// If the current state is not null then call exit and delete the object
	// and null the pointer.
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
	// Sets the current state to be the state sent as parameter and 
	// call Enter function on the current state.
	m_pxCurrentState = p_pxState;

	if (m_pxCurrentState != nullptr)
	{

		m_pxCurrentState->Enter();
	}

}


