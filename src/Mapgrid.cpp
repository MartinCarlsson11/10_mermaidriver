#include "stdafx.h"
#include <iostream>
#include <cstring>
MapGrid::MapGrid(float x, float y, float w, float h, System xSystem)
{
	MapGridRect.height = h;
	MapGridRect.width = w;
	MapGridRect.left = x;
	MapGridRect.top = y;

	m_xSystem = xSystem;

	m_xFrontSandSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/FrontSandTiles.png", 0, 0, 120, 120);
	m_xWaterSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/BackgroundTiles.png", 0, 0, 120, 120);
	m_xSurfaceSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/SurfaceTile.png", 0, 0, 120, 120);
	m_xBackSandSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/BackSandTiles.png", 0, 0, 120, 120);
	m_xMiddleSandSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/MiddleSandTiles.png", 0, 0, 120, 120);

	CreateMapGrid();
}

void MapGrid::Scroll(float xScrollAmount)
{
	MapGridRect.left -= xScrollAmount;
}

void MapGrid::Loop()
{
	if (MapGridRect.left + MapGridRect.width <= 0)
	{
		MapGridRect.left = 1920;
	}
}

sf::Sprite MapGrid::GetSprite()
{
	m_xSprite.setTextureRect(m_xTextureRect);
	return m_xSprite;
}

sf::IntRect MapGrid::GetTextureRect()
{
	return m_xTextureRect;
}

void MapGrid::Draw()
{

}

void MapGrid::CreateMapGrid()
{
	TiXmlDocument doc("../assets/Tiles.tmx");
	if (!doc.LoadFile())
	{
		std::cerr << doc.ErrorDesc() << std::endl;
	}
	TiXmlElement* root = doc.FirstChildElement();
	if (root == nullptr)
	{
		std::cerr << "Failed To load File:: No root element." << std::endl;
		doc.Clear();
	}
	int i = 0;
	const char* firstgid = nullptr;
	const char* tileWidth = nullptr;
	const char* tileHeight = nullptr;
	const char* tilecount = nullptr;
	const char* imagesource = nullptr;
	const char* width = nullptr;
	const char* height = nullptr;
	const char* name = nullptr;

	const char* gridWidth = nullptr;
	const char* gridHeight = nullptr;
	const char* gridOffsetX = nullptr;
	const char* gridOffsetY = nullptr;

	const char* mapdatainfo = nullptr;

	for (TiXmlElement* elem = root->FirstChildElement(); elem != nullptr; elem = elem->NextSiblingElement())
	{
		std::string elemName = elem->Value();

		if (elemName == "tileset")
		{
			firstgid = elem->Attribute("firstgid");

			name = elem->Attribute("name");

			tileWidth = elem->Attribute("tilewidth");

			tileHeight = elem->Attribute("tileheight");

			tilecount = elem->Attribute("tilecount");

			TiXmlElement* textureElem = elem->FirstChildElement();

			imagesource = textureElem->Attribute("source");

			width = textureElem->Attribute("width");

			height = textureElem->Attribute("height");

			m_xTileSet = new TileSet(atoi(firstgid), atoi(tileWidth), atoi(tileHeight), atoi(tilecount), atoi(width), atoi(height), name, imagesource, m_xSystem);
		}



		int xLayers = 0;
		if (elemName == "layer")
		{
			gridWidth = elem->Attribute("width");
			gridHeight = elem->Attribute("height");
			if (elem->Attribute("offsetx") != NULL)
			{
				gridOffsetX = elem->Attribute("offsetx");
			}
			if (elem->Attribute("offsety") != NULL)
			{
				gridOffsetY = elem->Attribute("offsety");
			}

			m_pxMapGrid.resize(xLayers + 1);

			int xArray = 0;
			for (TiXmlElement* dataElem = elem->FirstChildElement(); dataElem != nullptr; dataElem = dataElem->NextSiblingElement())
			{
				
				elemName = dataElem->Value();
				if (elemName == "data")
				{
					int xDataArray = 0;
					for (TiXmlElement* gidElem = dataElem->FirstChildElement(); gidElem != nullptr; gidElem = gidElem->NextSiblingElement())
					{
						elemName = gidElem->Value();
						if (elemName == "tile")
						{
							mapdatainfo = gidElem->Attribute("gid");

							m_xTiles[xArray].push_back(new Tile());
							m_xTiles[xArray][xDataArray]->gid = atoi(mapdatainfo);
							m_xTiles[xArray][xDataArray]->height = atoi(gridHeight);
							m_xTiles[xArray][xDataArray]->height = atoi(gridWidth);
							m_xTiles[xArray][xDataArray]->offsetx = atoi(gridOffsetX);
							m_xTiles[xArray][xDataArray]->offsety = atoi(gridOffsetY);
							xArray++;
						}
						xDataArray++;
					}
				}
			}
			xLayers++;
			
		}
		i++;
	}









}	