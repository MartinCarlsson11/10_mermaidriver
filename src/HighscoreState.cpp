#include "stdafx.h"
#include "Menu.h"
#include "HighscoreState.h"
#include <fstream>
#include "MenuState.h"
#include "LevelLoadState.h"

HighscoreState::HighscoreState(System &p_xSystem)
{
	m_xSystem = p_xSystem;
	mSelectedState = -1;
	mMenu = new Menu(m_xSystem, Button_MainMenu, sf::FloatRect(m_xSystem.m_iScreenWidth/2, m_xSystem.m_iScreenHeight -100, 0, 0));
}

HighscoreState::~HighscoreState()
{
	m_xSystem.m_pxSoundManager->StopMusic();

	delete mMenu;
	mMenu = nullptr;
}

void HighscoreState::Enter()
{
	SplashScreen = m_xSystem.m_pxSpriteManager->CreateSprite("..\\assets\\Menu\\highscore.png", 0, 0, 1920, 1080);
	m_xSystem.m_pxSoundManager->PlayMusic(m_xSystem.m_pxSoundManager->UnderwaterEffect);
	{
		std::ifstream filein("../assets/Highscore.txt");
		int i = 0;
		for (std::string line; std::getline(filein, line); )
		{
			Entries[i] = line;
			i++;
		}
	}

	font.loadFromFile("../assets/font/Lato-Regular.ttf");

	Entry2.setString(Entries[0] + "\t\t\t" + Entries[4]);
	Entry2.setFont(font);
	Entry2.setPosition(m_xSystem.m_iScreenWidth / 2, 375);
	
	Entry2.setOrigin(Entry2.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);


	Entry3.setString(Entries[1] + "\t\t\t" + Entries[5]);
	Entry3.setFont(font);
	Entry3.setPosition(m_xSystem.m_iScreenWidth / 2, 475);
	
	Entry3.setOrigin(Entry3.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);

	Entry4.setString(Entries[2] + "\t\t\t" + Entries[6]);
	Entry4.setFont(font);
	Entry4.setPosition(m_xSystem.m_iScreenWidth / 2, 575);
	
	Entry4.setOrigin(Entry4.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);

	Entry5.setString(Entries[3] + "\t\t\t" + Entries[7]);
	Entry5.setFont(font);
	Entry5.setPosition(m_xSystem.m_iScreenWidth / 2, 675);

	Entry5.setOrigin(Entry5.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);


	Entry1.setString("Score \t Name");
	Entry1.setFont(font);
	Entry1.setPosition(m_xSystem.m_iScreenWidth / 2, 275);
	Entry1.setOrigin(Entry1.getGlobalBounds().width / 2, Entry1.getGlobalBounds().height / 2);

}

void HighscoreState::Draw()
{
	m_xSystem.m_pxDrawManager->SetDefaultView();
	m_xSystem.m_pxDrawManager->Draw(SplashScreen, sf::Vector2f(0.0f, 0.0f));

	m_xSystem.m_pxDrawManager->Draw(*mMenu);


	m_xSystem.m_pxDrawManager->Draw(Entry1);
	m_xSystem.m_pxDrawManager->Draw(Entry2);
	m_xSystem.m_pxDrawManager->Draw(Entry3);
	m_xSystem.m_pxDrawManager->Draw(Entry4);
	m_xSystem.m_pxDrawManager->Draw(Entry5);
}

bool HighscoreState::Update(float p_fDeltaTime)
{
	if (!mMenu->Update())
	{
		return false;
	}
	

	return true;
}


void HighscoreState::Exit()
{

}

IState* HighscoreState::NextState()
{
	return new MenuState(m_xSystem);
}

