#include <fstream>
#include "ParallaxHandler.h"
ParallaxHandler::ParallaxHandler()
{
	std::ifstream read("../assets/scrollspeed.txt");

	float xValue[10];
	read >> xValue[0] >> xValue[1] >> xValue[2] >> xValue[3] >> xValue[4] >> xValue[5] >> xValue[6] >> xValue[7] >> xValue[8] >> xValue[9];
	ScrollSpeed.push_back(xValue[0]);
	ScrollSpeed.push_back(xValue[1]);
	ScrollSpeed.push_back(xValue[2]);
	ScrollSpeed.push_back(xValue[3]);
	ScrollSpeed.push_back(xValue[4]);
	ScrollSpeed.push_back(xValue[5]);
	ScrollSpeed.push_back(xValue[6]);
	ScrollSpeed.push_back(xValue[7]);
	ScrollSpeed.push_back(xValue[8]);
	ScrollSpeed.push_back(xValue[9]);

	read.close();
	for (int i = 0; i < 10; i++)
	{	
		sf::View* xLayers;
		LayerPositions.push_back(new sf::FloatRect(960, 540, 1920, 1080));
		xLayers = new sf::View(*LayerPositions[i]);
		Layers.push_back(xLayers);
	}
}
ParallaxHandler::~ParallaxHandler()
{
	auto it = LayerPositions.begin();
	while (it != LayerPositions.end())
	{
		delete *it;
		*it = nullptr;
		it++;
	}
	auto itx = Layers.begin();
	while (itx != Layers.end())
	{
		delete *itx;
		*itx = nullptr;
		itx++;
	}

}
///Layer information goes here
///1 = Parallax 1 Speed
///2 = Parallax 2 Speed
///3 = Parallax 3 Speed
///4 = Bakground Speed
///5 = Foreground Speed
float ParallaxHandler::GetScrollSpeed(int xArray)
{
	return ScrollSpeed[xArray - 1];
}

std::vector<sf::View*> ParallaxHandler::GetLayers()
{
	return Layers;
}

void ParallaxHandler::ScrollLayers(float YScroll)
{
	if (LayerPositions[7]->left > 27600)
	{
		for (auto&& ScrollSpeed : ScrollSpeed)
		{
			ScrollSpeed = 0;
		}
	}
	int i = 0;
	for (auto Layers: Layers)
	{
		Layers->setCenter(LayerPositions[i]->left, YScroll);

			LayerPositions[i]->left += ScrollSpeed[i];
			if (Layers->getCenter().y + (Layers->getSize().y *0.5) >= 1680)
			{
				int NewYPos = 1680 - (Layers->getSize().y *0.5);
				Layers->setCenter(LayerPositions[i]->left, NewYPos);
			}
			else if (Layers->getCenter().y - (Layers->getSize().y *0.5) <= 0)
			{
				int NewYPos = Layers->getSize().y *0.5;
				Layers->setCenter(LayerPositions[i]->left, NewYPos);
			}
			i++;
	}



}

void ParallaxHandler::ScrollY(Captain xCaptain)
{
	
	for (auto LayerPositions : LayerPositions)
	{
		if (xCaptain.GetPosition().y < 400)
		{
			if (LayerPositions->top > 0)
			{
				LayerPositions->top -= 2;
			}
		}
	
		else if(xCaptain.GetPosition().y > 400)
		{
			if (LayerPositions->top + 1080 < 1440)
			{
			}
		}
	}
	int i = 0;
	for (auto Layers : Layers)
	{
		Layers->reset(*LayerPositions[i]);
		i++;
	}
}