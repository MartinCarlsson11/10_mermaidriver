#include "stdafx.h"
#include "Menu.h"

Menu::Menu(System xSystem, ButtonType xType, sf::FloatRect xPos)
{
	Mouse::SetButton(sf::Mouse::Left, false);
	mType.push_back(xType);
	mSystem = xSystem;
	mSelectedItem = -1;
	CreateSprite();
	SetTypes();

	mSprite[0].setPosition(sf::Vector2f(xPos.left, xPos.top));
	mHighLightSprite[0].setPosition(sf::Vector2f(xPos.left, xPos.top));
	for (auto&& mSprite : mSprite)
	{
		mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	}
	for (auto&& mHighLightSprite : mHighLightSprite)
	{
		mHighLightSprite.setOrigin(mHighLightSprite.getGlobalBounds().width / 2, mHighLightSprite.getGlobalBounds().height / 2);
	}

}
Menu::Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3, ButtonType xType4, sf::FloatRect xPos4, ButtonType xType5, sf::FloatRect xPos5)
{
	Mouse::SetButton(sf::Mouse::Left, false);
	mType.push_back(xType1);
	mType.push_back(xType2);
	mType.push_back(xType3);
	mType.push_back(xType4);
	mType.push_back(xType5);
	mSelectedItem = -1;
	mSystem = xSystem;
	CreateSprite();
	SetTypes();

	mSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	mSprite[3].setPosition(sf::Vector2f(xPos4.left, xPos4.top));
	mSprite[4].setPosition(sf::Vector2f(xPos5.left, xPos5.top));

	mHighLightSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mHighLightSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mHighLightSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	mHighLightSprite[3].setPosition(sf::Vector2f(xPos4.left, xPos4.top));
	mHighLightSprite[4].setPosition(sf::Vector2f(xPos5.left, xPos5.top));
	for (auto&& mSprite : mSprite)
	{
		mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	}
	for (auto&& mHighLightSprite : mHighLightSprite)
	{
		mHighLightSprite.setOrigin(mHighLightSprite.getGlobalBounds().width / 2, mHighLightSprite.getGlobalBounds().height / 2);
	}
}
Menu::Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3, ButtonType xType4, sf::FloatRect xPos4)
{
	Mouse::SetButton(sf::Mouse::Left, false);
	mType.push_back(xType1);
	mType.push_back(xType2);
	mType.push_back(xType3);
	mType.push_back(xType4);

	mSelectedItem = -1;
	mSystem = xSystem;
	CreateSprite();
	SetTypes();
	mSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	mSprite[3].setPosition(sf::Vector2f(xPos4.left, xPos4.top));
	mHighLightSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mHighLightSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mHighLightSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	mHighLightSprite[3].setPosition(sf::Vector2f(xPos4.left, xPos4.top));
	for (auto&& mSprite : mSprite)
	{
		mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	}
	for (auto&& mHighLightSprite : mHighLightSprite)
	{
		mHighLightSprite.setOrigin(mHighLightSprite.getGlobalBounds().width / 2, mHighLightSprite.getGlobalBounds().height / 2);
	}
}
Menu::Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3)
{
	Mouse::SetButton(sf::Mouse::Left, false);
	mType.push_back(xType1);
	mType.push_back(xType2);
	mType.push_back(xType3);

	mSelectedItem = -1;
	mSystem = xSystem;
	CreateSprite();
	SetTypes();
	mSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	mHighLightSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mHighLightSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mHighLightSprite[2].setPosition(sf::Vector2f(xPos3.left, xPos3.top));
	for (auto&& mSprite : mSprite)
	{
		mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	}
	for (auto&& mHighLightSprite : mHighLightSprite)
	{
		mHighLightSprite.setOrigin(mHighLightSprite.getGlobalBounds().width / 2, mHighLightSprite.getGlobalBounds().height / 2);
	}
}
Menu::Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2)
{
	Mouse::SetButton(sf::Mouse::Left, false);
	mType.push_back(xType1);
	mType.push_back(xType2);

	mSelectedItem = -1;
	mSystem = xSystem;
	CreateSprite();
	SetTypes();
	mHighLightSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mHighLightSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	mSprite[0].setPosition(sf::Vector2f(xPos1.left, xPos1.top));
	mSprite[1].setPosition(sf::Vector2f(xPos2.left, xPos2.top));
	for (auto&& mSprite : mSprite)
	{
		mSprite.setOrigin(mSprite.getGlobalBounds().width / 2, mSprite.getGlobalBounds().height / 2);
	}
	for (auto&& mHighLightSprite : mHighLightSprite)
	{
		mHighLightSprite.setOrigin(mHighLightSprite.getGlobalBounds().width / 2, mHighLightSprite.getGlobalBounds().height / 2);
	}
}
Menu::~Menu()
{

}

Menu::Menu()
{

}
bool Menu::Update()
{
	int i = 0;
	for (auto && mActiveSprite : mActiveSprite)
	{
		if (mActiveSprite.getGlobalBounds().contains(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y))
		{
			if (Mouse::DidButtonGetReleased(sf::Mouse::Left))
			{
				if (mSelectedItem == i)
				{
					Mouse::SetButton(sf::Mouse::Left, false);
					return false;
				}
				else
				{
					Mouse::SetButton(sf::Mouse::Left, false);
				}
			}
			mSelectedItem = i;
		}
		if (mSelectedItem == i)
		{
			mActiveSprite = mHighLightSprite[i];
		}
		else
		{
			mActiveSprite = mSprite[i];
		}
		i++;
	}

	if (Keyboard::IsKeyDown(sf::Keyboard::Return))
	{
		Keyboard::SetKey(sf::Keyboard::Return, false);
		return false;
	}

	if (Keyboard::IsKeyDown(sf::Keyboard::Up))
	{
		MoveUp();
		Keyboard::SetKey(sf::Keyboard::Up, false);
	}
	if (Keyboard::IsKeyDown(sf::Keyboard::Down))
	{
		MoveDown();
		Keyboard::SetKey(sf::Keyboard::Down, false);
	}
	return true;
}

void Menu::MoveUp()
{
	mSelectedItem--;
	if (mSelectedItem < 0)
	{
		mSelectedItem = 0;
	}
}
void Menu::MoveDown()
{
	mSelectedItem++;
	if (mSelectedItem > mSprite.size()-1)
	{
		mSelectedItem = mSprite.size()-1;
	}
}

int Menu::ReturnSelectedItem()
{
	if (mSelectedItem >= 0)
	{
		return mSelectedItem;
	}
	else
		return mSelectedItem + 1;
}

void Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// apply the transform
	states.transform *= getTransform();

	// draw the vertex array
	for (auto&& mActiveSprite : mActiveSprite)
	{
		target.draw(mActiveSprite, states);
	}
	

}

void Menu::CreateSprite()
{
	for (auto&& mType : mType)
	{
		mSprite.push_back(mSystem.m_pxSpriteManager->CreateSprite("../assets/menu/buttons.png", 0, 0, 0, 0));
		mHighLightSprite.push_back(mSystem.m_pxSpriteManager->CreateSprite("../assets/menu/buttons.png", 0, 0, 0, 0));
		mActiveSprite.push_back(mSystem.m_pxSpriteManager->CreateSprite("../assets/menu/buttons.png", 0, 0, 0, 0));
	}
}
void Menu::SetTypes()
{
	int i = 0;
	for (auto&& mType : mType)
	{
		sf::IntRect xPos;
		sf::IntRect xPosHighlight;
		if (mType == Button_Play)
		{
			xPosHighlight = sf::IntRect(50, 75, 325, 90);
			xPos = sf::IntRect(445, 75, 325, 90);
		}	
		else if (mType == Button_HighScores)
		{
			xPosHighlight = sf::IntRect(60, 185, 300, 70);
			xPos = sf::IntRect(460, 185, 300, 75);
		}
		else if (mType == Button_Quit)
		{
			xPosHighlight = sf::IntRect(75, 265, 260, 60);
			xPos = sf::IntRect(475, 265, 260, 75);
		}
		else if (mType == Button_Continue)
		{
			
			xPosHighlight = sf::IntRect(115, 415, 525, 125);
			xPos = sf::IntRect(115, 590, 525, 125);
		}
		else if (mType == Button_MainMenu)
		{
			
			xPosHighlight = sf::IntRect(115, 815, 525, 125);
			xPos = sf::IntRect(115, 985, 525, 125);
		}
		
		mSprite[i].setTextureRect(xPos);
		mHighLightSprite[i].setTextureRect(xPosHighlight);
		i++;
	}
}