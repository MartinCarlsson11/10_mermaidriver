#include "DrawManager.h"
DrawManager::DrawManager(sf::RenderWindow* p_xWindow)
{
	m_pxWindow = p_xWindow ;
}

DrawManager::~DrawManager()
{

}

void DrawManager::Draw(sf::Sprite p_xSprite, sf::Vector2f p_xDestination)
{
	p_xSprite.setPosition(sf::Vector2f(p_xDestination));
	m_pxWindow->draw(p_xSprite);
}

void DrawManager::Draw(sf::Text p_xText, sf::Vector2f p_xDestination)
{
	p_xText.setPosition(sf::Vector2f(p_xDestination));
	m_pxWindow->draw(p_xText);
}

void DrawManager::Draw(sf::Drawable &x_rect)
{
	m_pxWindow->draw(x_rect);
}


void DrawManager::Clear()
{
	m_pxWindow->clear(sf::Color(0x11, 0x22, 0x33, 0xff));
}
void DrawManager::Display()
{
	m_pxWindow->display();
}

void DrawManager::SetView(sf::View p_xView)
{
	m_pxWindow->setView(p_xView);
}

void DrawManager::SetDefaultView()
{
	m_pxWindow->setView(m_pxWindow->getDefaultView());
}



