#include "stdafx.h"
#include "Captain.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "Harpoon.h"

Captain::Captain(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{
	CreateSprite();
	m_xHitPoints = 4;
	m_xAngle = 0;
	mCurrentFrames = mAnimationFrames_Idle;
	mCurrentArmFrames = mAnimationFrames_Arm_Idle;
	mAnimationIsNotIdle = false;
	mSprite = mSprite_Idle;
	mArm = mSprite_Arm_Idle;
	setThrowOnce = false;
}

sf::Vector2f Captain::mCurrentPosition = sf::Vector2f(0, 0);

Captain::~Captain()
{
	mAnimationFrames.~AnimateSprite();
	mAnimationFrames_Hurt.~AnimateSprite();
	mCurrentFrames.~AnimateSprite();
	mCurrentArmFrames.~AnimateSprite();

	mAnimationFrames_Arm_Idle.~AnimateSprite();

	mAnimationFrames_Arm_Empty.~AnimateSprite();


	mAnimationFrames_Arm_SwimForward.~AnimateSprite();

	mAnimationFrames_Arm_SwimForward_Empty.~AnimateSprite();
	mAnimationFrames_Arm_SwimForward_Reel.~AnimateSprite();
	mAnimationFrames_Arm_Reel.~AnimateSprite();
	mAnimationFrames_Arm_Throw.~AnimateSprite();

	mAnimationFrames_Electric.~AnimateSprite();
	mAnimationFrames_Idle.~AnimateSprite();
	mAnimationFrames_SwimForward.~AnimateSprite();
	mAnimationFrames_Throw.~AnimateSprite();
}
bool Captain::Update()
{
	return true;
}

bool Captain::Update(sf::View xView)
{

	mSprite.setOrigin(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2);
	mArm.setOrigin(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2);

	mCurrentPosition = mPosition;
	mSprite.setPosition(mPosition);
	mArm.setPosition(mSprite.getPosition().x , mSprite.getPosition().y);
	UpdateCollider();
	xDamageAccu += limitDamage.restart();
	xAccumulator += limitMovement.restart();

	sf::Vector2i Mouse = sf::Mouse::getPosition();
	sf::Vector2f WorldMouse = mSystem.m_pxWindow->mapPixelToCoords(Mouse, xView);
	
	
	if (xAccumulator.asSeconds() > 0.6f)
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && Harpoon::GetHarpoonState() == true)
		{
			if(setThrowOnce == false)
			{
				mSprite = mSprite_Throw;
				mCurrentFrames = mAnimationFrames_Windup;
				mCurrentArmFrames = mAnimationFrames_Arm_Throw;
				mAnimationIsNotIdle = true;
				setThrowOnce = true;
			}
			

			m_xAngle = (atan2(mPosition.y - WorldMouse.y, mPosition.x - WorldMouse.x) * 180 / 3.14) + 180;
			
			if (mSprite.getRotation() > 270 || mSprite.getRotation() < 90)
			{
				mSprite.setScale(1.f, 1.f);
			}
			else
			{
				mSprite.setScale(1.f, -1.f);
			}
			
			
		}
		else if (Mouse::DidButtonGetReleased(0))
		{
			m_xAngle = 0;
			Mouse::SetButton(0, false);
			mCurrentFrames = mAnimationFrames_Throw;
			setThrowOnce = false;
			//set idle animation to empty
			mSprite_Arm_Idle = mSprite_Arm_Idle_Empty;
			mSprite_Arm_SwimForward = mSprite_Arm_SwimForward_Empty;
		}
		else if(Mouse::DidButtonGetReleased(sf::Mouse::Right))
		{
			mAnimationIsNotIdle = true;
			mArm = mSprite_Arm_SwimForward_Reel;
			mCurrentArmFrames = mAnimationFrames_Arm_SwimForward_Reel;

			mSprite_Arm_Idle = mSprite_Arm_Idle_Harpoon;
			mSprite_Arm_SwimForward = mSprite_Arm_SwimForward_Harpoon;
			
			Mouse::SetButton(sf::Mouse::Right, false);
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{


			mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->SwimSound, true);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				mVelocity.y = -5.0f;
				mCurrentArmFrames = mAnimationFrames_Arm_SwimForward;
				mCurrentFrames = mAnimationFrames_SwimForward;
				mArm = mSprite_Arm_SwimForward;
				mSprite = mSprite_SwimForward;
				mAnimationIsNotIdle = true;
				mArm.setRotation(270);
				mArm.setScale(1.f, 1.f);
				mSprite.setRotation(270);
				mSprite.setScale(1.f, 1.f);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				mVelocity.y = +5.0f;
				mCurrentArmFrames = mAnimationFrames_Arm_SwimForward;
				mCurrentFrames = mAnimationFrames_SwimForward;
				mArm = mSprite_Arm_SwimForward;
				mSprite = mSprite_SwimForward;
				mAnimationIsNotIdle = true;
				mArm.setRotation(90);
				mArm.setScale(1.f, 1.f);
				mSprite.setRotation(90);
				mSprite.setScale(1.f, 1.f);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				mVelocity.x = 5.0f;
				mCurrentArmFrames = mAnimationFrames_Arm_SwimForward;
				mCurrentFrames = mAnimationFrames_SwimForward;
				mArm = mSprite_Arm_SwimForward;
				mSprite = mSprite_SwimForward;
				mAnimationIsNotIdle = true;
				mArm.setRotation(0);
				mArm.setScale(1.f, 1.f);
				mSprite.setRotation(0);
				mSprite.setScale(1.f, 1.f);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				mVelocity.x = -5.0f;
				mCurrentArmFrames = mAnimationFrames_Arm_SwimForward;
				mCurrentFrames = mAnimationFrames_SwimForward;
				mArm = mSprite_Arm_SwimForward;
				mSprite = mSprite_SwimForward;
				mAnimationIsNotIdle = true;
				mArm.setRotation(180);
				mArm.setScale(1.f, -1.f);
				mSprite.setRotation(180);
				mSprite.setScale(1.f, -1.f);
			}
			limitMovement.restart();
			xAccumulator = sf::Time::Zero;
		}
		else if (mAnimationIsNotIdle)
		{
			if (mCurrentFrames.AnimationFinish())
			{
				mSprite = mSprite_Idle;
				mCurrentFrames = mAnimationFrames_Idle;
				mArm = mSprite_Arm_Idle;
				mCurrentArmFrames = mAnimationFrames_Arm_Idle;
				mAnimationIsNotIdle = false;
			}
			

		}
	}
	if (Mouse::DidButtonGetPressed(sf::Mouse::Left))
	{
		Mouse::SetPressButton(sf::Mouse::Left, false);
	}

	

	mSprite.setTextureRect(mCurrentFrames.UpdateASprite(0.016));

	mArm.setTextureRect(mCurrentArmFrames.UpdateASprite(0.016));

	//mSprite.setRotation(m_xAngle);


	if (xDamageAccu.asSeconds() > 2.0f)
	{

	}

	Move();
	Deacceleration();



	if (m_xHitPoints == 0)
	{
		return false;
	}
	return true;
}

sf::Sprite Captain::GetSprite()
{
	return mSprite;
}

void Captain::Move()
{
	mPosition += mVelocity;
	
	if (mPosition.x < 100)
	{
		mPosition.x = 100;
	}
	if (mPosition.y < 719)
	{
		mPosition.y = 719;
	}
	if (mPosition.x + mSprite.getLocalBounds().width/4 > mSystem.m_iScreenWidth)
	{
		mPosition.x = mSystem.m_iScreenWidth - mSprite.getLocalBounds().width/4;
	}
	if (mPosition.y + mSprite.getLocalBounds().height > mSystem.m_iScreenHeight +690)
	{
		mPosition.y = mSystem.m_iScreenHeight + 690 - mSprite.getLocalBounds().height;
	}
	
}


void Captain::RestoreHealth(int p_HealthRestore)
{
	m_xHitPoints += p_HealthRestore;
	mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->CaptainDrinkSound, true); 
}

int Captain::GetHP()
{
	return m_xHitPoints;
}

void Captain::TakeDamage(int p_damage, bool xElectric)
{

	

	if (xDamageAccu.asSeconds() > 1.5f)
	{
		if (xElectric == true)
		{
			mSprite = mSprite_Electric;
			mCurrentFrames = mAnimationFrames_Electric;
			mCurrentArmFrames = mAnimationFrames_Arm_Throw;
			mArm = mSprite_Throw;
			mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->CaptainElectricDamageSound, true);
		}
		else
		{
			mSprite = mSprite_Hurt;
			mCurrentFrames = mAnimationFrames_Hurt;
			mCurrentArmFrames = mAnimationFrames_Arm_Throw;
			mArm = mSprite_Throw;
			mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->CaptainTakeDamageSound, true);
		}
		m_xHitPoints -= p_damage;
		limitDamage.restart();
		xDamageAccu = sf::Time::Zero;

	}
}

void Captain::Deacceleration()
{
	if (mVelocity.x > 0.05f)
	{
		mVelocity.x -= 0.05f;
	}
	else if (mVelocity.x < -0.05f)
	{
		mVelocity.x += 0.05f;
	}
	else
	{
		mVelocity.x = 0.0f;
	}

	if (mVelocity.y > 0.05f)
	{
		mVelocity.y -= 0.05f;
	}
	else if (mVelocity.y < -0.05f)
	{
		mVelocity.y += 0.05f;
	}
	else
	{
		mVelocity.y = 0.0f;
	}
}

sf::Vector2f Captain::GetPosition()
{
	return mPosition;

}

void Captain::CreateSprite()
{
	mSprite_Arm_Idle_Empty = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_idle arm empty.PNG", 0, 0, 0, 0);
	mSprite_Arm_Idle_Empty.setOrigin(mSprite_Arm_Idle_Empty.getGlobalBounds().width / 2, mSprite_Arm_Idle_Empty.getGlobalBounds().height / 2);
	
	

	

	mSprite_Arm_Idle_Harpoon = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_idle arm.PNG", 0, 0, 0, 0);
	mSprite_Arm_Idle_Harpoon.setOrigin(mSprite_Arm_Idle_Harpoon.getGlobalBounds().width / 2, mSprite_Arm_Idle_Harpoon.getGlobalBounds().height / 2);

	mSprite_Arm_Idle = mSprite_Arm_Idle_Harpoon;

	mSprite_Arm_SwimForward_Empty = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_swim_forward arm empty.PNG", 0, 0, 0, 0);
	mSprite_Arm_SwimForward_Empty.setOrigin(mSprite_Arm_SwimForward_Empty.getGlobalBounds().width / 2, mSprite_Arm_SwimForward_Empty.getGlobalBounds().height / 2);

	mSprite_Arm_SwimForward_Reel = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_swim_forward arm reel.PNG", 0, 0, 0, 0);
	mSprite_Arm_SwimForward_Reel.setOrigin(mSprite_Arm_SwimForward_Reel.getGlobalBounds().width / 2, mSprite_Arm_SwimForward_Reel.getGlobalBounds().height / 2);

	mSprite_Arm_SwimForward_Harpoon = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_swim_forward arm.PNG", 0, 0, 0, 0);
	mSprite_Arm_SwimForward_Harpoon.setOrigin(mSprite_Arm_SwimForward_Harpoon.getGlobalBounds().width / 2, mSprite_Arm_SwimForward_Harpoon.getGlobalBounds().height / 2);

	mSprite_Arm_SwimForward = mSprite_Arm_SwimForward_Harpoon;

	mSprite_Arm_Reel = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_swim_forward arm.PNG", 0, 0, 0, 0);
	mSprite_Arm_Reel.setOrigin(mSprite_Arm_Reel.getGlobalBounds().width / 2, mSprite_Arm_Reel.getGlobalBounds().height / 2);

	mSprite_Hurt = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/Cpt_hurt.png", 0, 0, 0, 0);
	mSprite_Hurt.setOrigin(mSprite_Hurt.getGlobalBounds().width / 2, mSprite_Hurt.getGlobalBounds().height / 2);
	mSprite_Hurt.setScale(0.5f, 0.5f);

	mSprite_Electric = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_electric.PNG", 0,0,0,0);
	mSprite_Electric.setOrigin(mSprite_Electric.getGlobalBounds().width / 2, mSprite_Electric.getGlobalBounds().height / 2);

	mSprite_Idle = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_idle.PNG", 0, 0, 0, 0);
	mSprite_Idle.setOrigin(mSprite_Idle.getGlobalBounds().width / 2, mSprite_Idle.getGlobalBounds().height / 2);

	mSprite_SwimForward = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_swim forward.PNG", 0, 0, 0, 0);
	mSprite_SwimForward.setOrigin(mSprite_SwimForward.getGlobalBounds().width / 2, mSprite_SwimForward.getGlobalBounds().height / 2);

	mSprite_Throw = mSystem.m_pxSpriteManager->CreateSprite("../assets/captain/cpt_throw.PNG", 0, 0, 0, 0);
	mSprite_Throw.setOrigin(mSprite_Throw.getGlobalBounds().width / 2, mSprite_Throw.getGlobalBounds().height / 2);

	mAnimationFrames_Hurt.AddFrame(0, 0, 528, 239, 0);
	mAnimationFrames_Hurt.AddFrame(0, 0, 528, 239, 5);


	mAnimationFrames_Arm_Idle.AddFrame(0, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(311, 0,311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(622, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(933, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(1244, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(1555, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Idle.AddFrame(1866, 0, 311, 177, 0.1);
	
	mAnimationFrames_Arm_Throw.AddFrame(0, 0, 0, 0, 8);

	mAnimationFrames_Arm_SwimForward.AddFrame(0, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(342, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(684, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(1026, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(1368, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(1710, 0, 342, 177, 0.1);
	mAnimationFrames_Arm_SwimForward.AddFrame(2052, 0, 342, 177, 0.1);

	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(0, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(311, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(622, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(933, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(1244, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(1555, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_SwimForward_Empty.AddFrame(1866, 0, 311, 177, 0.1);

	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(0, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(342, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(684, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(1026, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(1368, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(1710, 0, 342, 177, 0.2);
	mAnimationFrames_Arm_SwimForward_Reel.AddFrame(2052, 0, 342, 177, 0.2);

	mAnimationFrames_Arm_Reel.AddFrame(0, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(311, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(622, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(933, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(1244, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(1555, 0, 311, 177, 0.1);
	mAnimationFrames_Arm_Reel.AddFrame(1866, 0, 311, 177, 0.1);

	mAnimationFrames_Electric.AddFrame(0, 0, 311, 177, 0.1);
	mAnimationFrames_Electric.AddFrame(311, 0, 311, 177, 0.1);
	mAnimationFrames_Electric.AddFrame(622, 0, 311, 177, 0.1);
	mAnimationFrames_Electric.AddFrame(933, 0, 311, 177, 0.1);

	mAnimationFrames_Idle.AddFrame(0, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(311, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(622, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(933, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(1244, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(1555, 0, 311, 177, 0.12);
	mAnimationFrames_Idle.AddFrame(1866, 0, 311, 177, 0.12);
	

	mAnimationFrames_SwimForward.AddFrame(0, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(342, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(684, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(1026, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(1368, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(1710, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(2052, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(2394, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(2736, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(3078, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(3420, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(3762, 0, 342, 177, 0.07);
	mAnimationFrames_SwimForward.AddFrame(4104, 0, 342, 177, 0.07);


	mAnimationFrames_Windup.AddFrame(0, 0, 368, 177, 0.1);
	mAnimationFrames_Windup.AddFrame(368, 0, 368, 177, 0.1);
	mAnimationFrames_Windup.AddFrame(736, 0, 368, 177, 100);

	mAnimationFrames_Throw.AddFrame(1104, 0, 368, 177, 0.05);
	mAnimationFrames_Throw.AddFrame(1472, 0, 368, 177, 0.05);
	mAnimationFrames_Throw.AddFrame(1840, 0, 368, 177, 0.05);
	mAnimationFrames_Throw.AddFrame(2208, 0, 368, 177, 0.05);
	
}

bool Captain::BubblePowerup()
{
	return m_xBubblePowerup;
}

void Captain::SetPowerUpActive(bool xState)
{
	m_xBubblePowerup = xState;
}

void Captain::UpdateCollider()
{
	sf::RectangleShape CaptainHitBox;
	CaptainHitBox.setSize(sf::Vector2f(150, 75));
	CaptainHitBox.setOrigin(sf::Vector2f(75.f, 37.5f));
	CaptainHitBox.setPosition(sf::Vector2f(mPosition.x, mPosition.y));

	SetColliderRect1(CaptainHitBox);
}


void Captain::draw(sf::RenderTarget & target,sf::RenderStates states)		const
{
	// apply the transform
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();


	
	target.draw(mSprite, states);

	
	states.texture = mArm.getTexture();

	target.draw(mArm, states);
}

void Captain::SetNewAnimation(AnimateSprite xAnimateSprite)
{
	mAnimationFrames = xAnimateSprite;
}


EntityType Captain::GetType()
{
	return mType;
}


sf::Vector2f Captain::GetCaptainPosition()
{
	return mCurrentPosition;
}