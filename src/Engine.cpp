#include "stdafx.h"
#include "Engine.h"
#include "IState.h"
#include <iostream>
#include "PreloadState.h"

Engine::Engine()
{
	m_bRunning = false;
	m_pxSpriteManager = nullptr;
	m_pxDrawManager = nullptr;
	m_pxWindow = nullptr;
	m_pxSoundManager = nullptr;
	TextEnteredString.reserve(6);
}

Engine::~Engine()
{
}

bool Engine::Init()
{
	m_pxWindow = new sf::RenderWindow();
	m_pxWindow->create(sf::VideoMode(1920, 1080), "Mermaid River", sf::Style::Titlebar | sf::Style::Close | sf::Style::Fullscreen);
	m_bRunning = true;
	m_pxWindow->setVerticalSyncEnabled(true);
	m_pxWindow->setKeyRepeatEnabled(false);

	//initialize our managers
	{
		m_pxSoundManager = new SoundManager();
		m_pxSpriteManager = new SpriteManager();
		m_pxDrawManager = new DrawManager(m_pxWindow);
		m_pxStateManager = new StateManager();
	}
	System xSystem;
	int ScreenHeight = 1080, ScreenWidth = 1920;
	xSystem.m_xEngine = this;
	xSystem.m_pxWindow = m_pxWindow;
	xSystem.m_pxStateManager = m_pxStateManager;
	xSystem.m_pxDrawManager = m_pxDrawManager;
	xSystem.m_pxSpriteManager = m_pxSpriteManager;
	xSystem.m_pxSoundManager = m_pxSoundManager;
	xSystem.m_iScreenHeight = ScreenHeight;
	xSystem.m_iScreenWidth = ScreenWidth;
	m_pxStateManager->SetState(new Preload(xSystem));

	return true;
}

void Engine::Update()
{
	sf::Clock deltaClock;
	//Target fps set to 60 fps.
	m_xTargetTime = sf::seconds( 1.0f / 60.0f);
	m_xAccumulator = sf::Time::Zero;
	static unsigned int frameCounter = 0;
	static unsigned int frameTime = 0;
	static unsigned int fps = 0;
	while (m_bRunning == true)
	{
		///handle events outside of 60fps restriction to avoid input lag
		HandleEvents();

		m_pxDrawManager->Clear();
		
		//makes sure we always update logic at 60fps. whilst drawing whenever we have time to draw. 
		///Update Logic
		while (m_xAccumulator > m_xTargetTime)
		{
			m_xAccumulator -= m_xTargetTime;
			if (!m_pxStateManager->Update())
			{
				m_bRunning = false;
			}

		
		}
		m_xAccumulator += m_xClock.restart();
		m_pxStateManager->Draw();
		m_pxDrawManager->Display();

		frameCounter++;
		frameTime += deltaClock.restart().asMilliseconds();
		if (frameTime >= 1000)
		{
			fps = frameCounter;
			frameCounter = 0;
			frameTime -= 1000;
			std::cout << fps << std::endl;
		}
	}
}
void Engine::Shutdown()
{
	delete m_pxStateManager;
	m_pxStateManager = nullptr;

	delete m_pxDrawManager;
	m_pxDrawManager = nullptr;

	delete m_pxSpriteManager;
	m_pxSpriteManager = nullptr;

	delete m_pxSoundManager;
	m_pxSoundManager = nullptr;

	delete m_pxWindow;
	m_pxWindow = nullptr;

}
void Engine::HandleEvents()
{
	while (m_pxWindow->pollEvent(m_xEvent))
	{
		if (m_xEvent.type == sf::Event::MouseButtonPressed)
		{
			Mouse::SetPressButton(m_xEvent.mouseButton.button, true);
		}
		if (m_xEvent.type == sf::Event::MouseButtonReleased)
		{
			Mouse::SetButton(m_xEvent.mouseButton.button, true);
		}

		if (m_xEvent.type == sf::Event::KeyReleased)
		{
			if (m_xEvent.key.code == sf::Keyboard::Up)
			{
				Keyboard::SetKey(m_xEvent.key.code, false);
			}
			if (m_xEvent.key.code == sf::Keyboard::Down)
			{
				Keyboard::SetKey(m_xEvent.key.code, false);
			}
		}


		if (m_xEvent.type == sf::Event::KeyPressed)
		{
			if (m_xEvent.key.code == sf::Keyboard::Return)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			if (m_xEvent.key.code == sf::Keyboard::BackSpace)
			{
				if (TextEnteredString.size() > 0)
				{
					TextEnteredString.erase(TextEnteredString.size() - 1);
				}
			}

			if (m_xEvent.key.code == sf::Keyboard::Escape)
			{
				m_pxWindow->close();
				m_bRunning = false;
			}
			if (m_xEvent.key.code == sf::Keyboard::W)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			if (m_xEvent.key.code == sf::Keyboard::A)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			if (m_xEvent.key.code == sf::Keyboard::S)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			if (m_xEvent.key.code == sf::Keyboard::D)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}


			if (m_xEvent.key.code == sf::Keyboard::Up)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			if (m_xEvent.key.code == sf::Keyboard::Down)
			{
				Keyboard::SetKey(m_xEvent.key.code, true);
			}
			
		}
		if (m_xEvent.type == sf::Event::TextEntered)
		{
			if (m_xEvent.text.unicode < 128 )
			{
				if (m_xEvent.text.unicode != 8 || m_xEvent.text.unicode != 28)
				{
					TextEnteredString += (char)m_xEvent.text.unicode;
				}
			}
		}
	}
}

std::string Engine::GetTextEntered()
{
	return TextEnteredString;
}
void Engine::ResetTextEntered()
{
	TextEnteredString = " ";
}