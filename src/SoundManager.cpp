#include "SoundManager.h"
#include <random>
SoundManager::SoundManager()
{
	//Captain
	SwimBuffer.loadFromFile("../assets/Captain/sounds/Swimsound.wav");
	SwimSound.setBuffer(SwimBuffer);

	Birdattack.loadFromFile("../assets/Level1/Bird/Sound/attack.wav");
	BirdDeath.loadFromFile("../assets/Level1/Bird/Sound/Death.wav");
	BirdSwim.loadFromFile("../assets/Level1/Bird/Sound/Swim.wav");
	BirdWater.loadFromFile("../assets/Level1/Bird/Sound/Water Splash.wav");

	Blowfishdeath.loadFromFile("../assets/Level1/blowfish/Sound/death.wav");
	Blowfishswim.loadFromFile("../assets/Level1/blowfish/Sound/swim.wav");
	
	CoinPickup.loadFromFile("../assets/Level1/coins/Sound/Pickup.wav");

	Flyfishattack.loadFromFile("../assets/Level1/flyfish/Sound/attack.wav");
	Flyfishswim.loadFromFile("../assets/Level1/flyfish/Sound/swim.wav");

	JellyfishAttack.loadFromFile("../assets/Level1/jellyfish/Sound/Death.wav");
	JellyfishSwim.loadFromFile("../assets/Level1/jellyfish/Sound/electricity.wav");
	JellyfishDeath.loadFromFile("../assets/Level1/jellyfish/Sound/swim.wav");

	SwordfishDeath.loadFromFile("../assets/Level1/Swordfish/Sound/Death.wav");
	SwordfishLunge.loadFromFile("../assets/Level1/Swordfish/Sound/Lunge.wav");
		
	PowerupBubbleBuffer.loadFromFile("../assets/Level1/powerups/Sound/Pickup bubble.wav");
	PowerupSwordBuffer.loadFromFile("../assets/Level1/powerups/Sound/Pickup chain.wav");
	PowerupChainBuffer.loadFromFile("../assets/Level1/powerups/Sound/Pickup sword.wav");

	CaptainRefill.loadFromFile("../assets/Captain/Sounds/Refill air.wav");
	CaptainReel.loadFromFile("../assets/Captain/Sounds/Reel in.wav");
	CaptainTakeDamage.loadFromFile("../assets/Captain/Sounds/TakeDamage.wav");
	CaptainThrow.loadFromFile("../assets/Captain/Sounds/Throw.wav");
	CaptainDeath.loadFromFile("../assets/Captain/Sounds/Death.wav");

	HarpoonChargeComplete.loadFromFile("../assets/captain/sounds/Fully loaded.wav");

	CaptainBirdDamage.loadFromFile("../assets/Captain/Sounds/Cpt_damage_bird.wav");
	CaptainElectricDamage.loadFromFile("../assets/Captain/Sounds/Cpt_damage_jellyfish.wav");
	CaptainDrink.loadFromFile("../assets/Captain/Sounds/Cpt_drinkfinal.wav");


	BirdattackSound.setBuffer(Birdattack);
	BirdattackSound.setVolume(50);
	BirdDeathSound.setBuffer(BirdDeath);
	BirdSwimSound.setBuffer(BirdSwim);
	BirdWaterSound.setBuffer(BirdWater);

	BlowfishdeathSound.setBuffer(Blowfishdeath);
	BlowfishswimSound.setBuffer(Blowfishswim);

	CoinPickupSound.setBuffer(CoinPickup);

	FlyfishswimSound.setBuffer(Flyfishswim);

	JellyfishAttackSound.setBuffer(JellyfishAttack);
	JellyfishSwimSound.setBuffer(JellyfishSwim);
	JellyfishDeathSound.setBuffer(JellyfishDeath);

	SwordfishDeathSound.setBuffer(SwordfishDeath);
	SwordfishLungeSound.setBuffer(SwordfishLunge);

	PowerupBubbleBufferSound.setBuffer(PowerupBubbleBuffer);
	PowerupSwordBufferSound.setBuffer(PowerupSwordBuffer);
	PowerupChainBufferSound.setBuffer(PowerupChainBuffer);

	CaptainRefillSound.setBuffer(CaptainRefill);
	CaptainReelSound.setBuffer(CaptainReel);
	CaptainTakeDamageSound.setBuffer(CaptainTakeDamage);
	CaptainThrowSound.setBuffer(CaptainThrow);
	CaptainDeathSound.setBuffer(CaptainDeath);
	CaptainDrinkSound.setBuffer(CaptainDrink);;
	CaptainBirdDamageSound.setBuffer(CaptainBirdDamage);;
	CaptainElectricDamageSound.setBuffer(CaptainElectricDamage);;
	HarpoonFullChargeSound.setBuffer(HarpoonChargeComplete);
	Music.openFromFile("../assets/level1/music/music.wav");
	Music.setVolume(100);
	Music.setLoop(true);

	DeathMusic.openFromFile("../assets/menu/sounds/Fail.wav");
	DeathMusic.setLoop(true);

	WinMusic.openFromFile("../assets/menu/sounds/victory.wav");

	UnderwaterEffect.openFromFile("../assets/level1/music/UnderwaterEffect.wav");
	UnderwaterEffect.setLoop(true);
} 

SoundManager::~SoundManager()
{

}

void SoundManager::PlaySound(sf::Sound& xSound, bool xPitch)
{
	//randomize pitch
	if (xPitch == true)
	{
		std::random_device device;

		std::mt19937 generator(device());

		std::uniform_real_distribution<> Distribution(.95f, 1.1f);
		xSound.setPitch(Distribution(generator));
	}
	xSound.play();
}

void SoundManager::PlayMusic(sf::Music& xMusic)
{
	xMusic.play();
}

void SoundManager::StopMusic()
{
	Music.stop();
	DeathMusic.stop();
	UnderwaterEffect.stop();
}