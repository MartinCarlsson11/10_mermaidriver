
#include "AnimateSprite.h"
AnimateSprite::AnimateSprite()
{
	AnimationFinished = false;
}


AnimateSprite::~AnimateSprite()
{
	m_axFrames.clear();
}

void AnimateSprite::AddFrame(int p_iX, int p_iY, int p_iWidth, int p_iHeight, float p_fDuration)
{
	Myframes.m_xRegion.left = p_iX;
	Myframes.m_xRegion.top = p_iY;
	Myframes.m_xRegion.width = p_iWidth;
	Myframes.m_xRegion.height = p_iHeight;
	Myframes.m_fDuration = p_fDuration;
	m_axFrames.push_back(Myframes);
}

void AnimateSprite::AddFramesFromFile(std::vector<FrameData> xFrames)
{
	m_axFrames = xFrames;
}

sf::IntRect AnimateSprite::UpdateASprite(float p_fDeltaTime)
{
	AnimationFinished = false;
	m_fCurrentDuration += p_fDeltaTime;

	if (m_fCurrentDuration >= m_axFrames[m_iIndex].m_fDuration)
	{

		if (m_iIndex < m_axFrames.size() - 1)
		{
			m_iIndex++;
		}
		else
		{
			AnimationFinished = true;
			m_iIndex = 0;
		}
		m_fCurrentDuration = 0.0f;
		return m_axFrames[m_iIndex].m_xRegion;
	}
	else return m_axFrames[m_iIndex].m_xRegion;
}

sf::IntRect AnimateSprite::UpdateASprite(float p_fDeltaTime, bool xRepeat)
{
	AnimationFinished = false;
	m_fCurrentDuration += p_fDeltaTime;

	if (m_fCurrentDuration >= m_axFrames[m_iIndex].m_fDuration)
	{

		if (m_iIndex < m_axFrames.size() - 1)
		{
			m_iIndex++;
		}
		else
		{
			AnimationFinished = true;
			if (xRepeat == false)
			{
				m_iIndex = 0;
			}
			
		}

		if (xRepeat == false)
		{
			m_fCurrentDuration = 0.0f;
		}

	
		return m_axFrames[m_iIndex].m_xRegion;
	}

	
	else
	{
		return m_axFrames[m_iIndex].m_xRegion;
	}
}

bool AnimateSprite::AnimationFinish()
{
	if (AnimationStopped)
	{
		return true;
	}
	else
	{
		return AnimationFinished;
	}
}


void AnimateSprite::AnimationStop()
{
	AnimationStopped = true;
}
void AnimateSprite::AnimationResume()
{
	AnimationStopped = false;
}

void AnimateSprite::Reset()
{
	m_fCurrentDuration = 0;
	m_iIndex = 0;
}