#include "stdafx.h"
#include "Menu.h"
#include "MenuState.h"
#include "HighscoreState.h"
#include "GameState.h"
MenuState::MenuState(System &p_xSystem)
{
	m_xSystem = p_xSystem;

	mSelectedState = -1;
	mMenu = Menu(m_xSystem, Button_Play, sf::FloatRect(m_xSystem.m_iScreenWidth/2 , m_xSystem.m_iScreenHeight -275, 0, 0), 
		Button_HighScores, sf::FloatRect(m_xSystem.m_iScreenWidth / 2, m_xSystem.m_iScreenHeight - 150, 0, 0), 
		Button_Quit, sf::FloatRect(m_xSystem.m_iScreenWidth / 2, m_xSystem.m_iScreenHeight - 50, 0, 0));
}

MenuState::~MenuState()
{
	m_xSystem.m_pxSoundManager->StopMusic();
}

void MenuState::Enter()
{
	SplashScreen = m_xSystem.m_pxSpriteManager->CreateSprite("..\\assets\\Menu\\splash.png",0,0,1920, 1080);
	m_xSystem.m_pxSoundManager->PlayMusic(m_xSystem.m_pxSoundManager->UnderwaterEffect);
}

void MenuState::Draw()
{
	m_xSystem.m_pxDrawManager->SetDefaultView();
	m_xSystem.m_pxDrawManager->Draw(SplashScreen, sf::Vector2f(0.0f, 0.0f));

	m_xSystem.m_pxDrawManager->Draw(mMenu);
}

bool MenuState::Update(float p_fDeltaTime)
{
	if (!mMenu.Update())
	{
		mSelectedState = mMenu.ReturnSelectedItem();
		return false;
	}
	return true;
}


void MenuState::Exit()
{

}

IState* MenuState::NextState()
{
	if (mSelectedState == 0)
	{
		return new GameState(m_xSystem);
	}
	else if (mSelectedState == 1)
	{
		return new HighscoreState(m_xSystem);
	}
	else if (mSelectedState == 2)
	{
		return nullptr;
	}
}	

