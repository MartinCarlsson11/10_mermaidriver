#include "Coins.h"

Coins::Coins(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
	CreateSprite();
}

Coins::~Coins()
{
	mAnimationFrames.~AnimateSprite();
}

bool Coins::Update()
{
	mSprite.setPosition(mPosition);
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(32, 32));
	xCollider.setPosition(sf::Vector2f(mPosition.x, mPosition.y));
	SetColliderRect1(xCollider);

	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.016f));

	Move();
	if (mPosition.x < 0 - 120)
	{
		return false;
	}
	else
		return true;
}
void Coins::Move()
{
	mPosition += mVelocity;
}

sf::Vector2f Coins::GetPosition()
{
	return mPosition;
}
sf::Sprite Coins::GetSprite()
{
	return mSprite;
}

EntityType Coins::GetType()
{
	return mType;
}


void Coins::CreateSprite()
{
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/coins/coin.png", 0, 0, 32, 32);
	mAnimationFrames.AddFrame(0, 0, 32, 32, 0.1f);
	mAnimationFrames.AddFrame(32, 0, 32, 32, 0.1f);
	mAnimationFrames.AddFrame(64, 0, 32, 32, 0.1f);
	mAnimationFrames.AddFrame(0, 32, 32, 32, 0.1f);
	mAnimationFrames.AddFrame(32, 32, 32, 32, 0.1f);
	mAnimationFrames.AddFrame(64, 32, 32, 32, 0.1f);

	mAnimationFrames.AddFrame(0, 64, 32, 32, 3.f);


}


void Coins::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}
