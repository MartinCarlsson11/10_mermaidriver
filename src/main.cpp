#include "Engine.h"

int main(int argc, char** argv)
{
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	
#endif 
	Engine xEngine;
	if(xEngine.Init() != false)
	{
		xEngine.Update();
	}
	xEngine.Shutdown();
	return 0;
}