#include "Mouse.h"

Mouse::Mouse()
{

}

Mouse::~Mouse()
{
}

bool Mouse::m_abButtons[3]{ false };
bool Mouse::m_abPressButtons[3]{ false };

bool Mouse::DidButtonGetPressed(int p_iIndex)
{
	if (p_iIndex < 0)
		return false;
	if (p_iIndex > 2)
		return false;

	return m_abPressButtons[p_iIndex];
}

bool Mouse::DidButtonGetReleased(int p_iIndex)
{
	if (p_iIndex < 0)
		return false;
	if (p_iIndex > 2)
		return false;

	return m_abButtons[p_iIndex];
}
 
void Mouse::SetButton(int p_iIndex,
	bool p_bValue)
{
	m_abButtons[p_iIndex] = p_bValue;
}

void Mouse::SetPressButton(int p_iIndex,
	bool p_bValue)
{
	m_abPressButtons[p_iIndex] = p_bValue;
}