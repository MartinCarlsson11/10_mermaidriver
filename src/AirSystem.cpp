#include "stdafx.h"
#include "AirSystem.h"
#include "CollisionManager.h"
#include "Captain.h"

AirSystem::AirSystem()
{
	AirLoss = true;
	MaxAirLevel = 30.f;
	AirLevel = 30.f;
}

AirSystem::~AirSystem()
{

}

void AirSystem::Update(Captain xCaptain)
{
	sf::RectangleShape Surface;
	Surface.setPosition(0.f, 600.f);
	Surface.setSize(sf::Vector2f(1920.f, 100.f));

	if (xCaptain.BubblePowerup())
	{
		AirLoss = false;
	}
	else if (CollisionManager::CheckCollisionAABB(xCaptain.GetColliderRect1(), Surface))
	{
		AirLoss = false;
	}
	else
	{
		AirLoss = true;
	}

	if (AirLoss == true)
	{
		AirLevel -= 0.016;
	}
	else
	{
		AirLevel += 1;
	}

	if (AirLevel < 0)
	{
		AirLevel = 0;
	}
	if (AirLevel > MaxAirLevel)
	{
		AirLevel = MaxAirLevel;
	}
}

float AirSystem::GetAirLevel()
{
	return AirLevel;
}