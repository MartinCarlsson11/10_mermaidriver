#include "CollisionManager.h"

bool CollisionManager::CheckCollisionAABB(sf::RectangleShape xCollider1, sf::RectangleShape xCollider2)
{
	if (xCollider1.getGlobalBounds().left < xCollider2.getGlobalBounds().left + xCollider2.getGlobalBounds().width &&
		xCollider1.getGlobalBounds().left + xCollider1.getGlobalBounds().width > xCollider2.getGlobalBounds().left &&
		xCollider1.getGlobalBounds().top < xCollider2.getGlobalBounds().top + xCollider2.getGlobalBounds().height &&
		xCollider1.getGlobalBounds().top + xCollider1.getGlobalBounds().height > xCollider2.getGlobalBounds().top)
	{
		return true;
	}
	else return false;
}


bool CollisionManager::CheckCollisionAABB(sf::Sprite xCollider1, sf::RectangleShape xCollider2)
{
	if (xCollider1.getGlobalBounds().left < xCollider2.getGlobalBounds().left + xCollider2.getGlobalBounds().width &&
		xCollider1.getGlobalBounds().left + xCollider1.getGlobalBounds().width > xCollider2.getGlobalBounds().left &&
		xCollider1.getGlobalBounds().top < xCollider2.getGlobalBounds().top + xCollider2.getGlobalBounds().height &&
		xCollider1.getGlobalBounds().top + xCollider1.getGlobalBounds().height > xCollider2.getGlobalBounds().top)
	{
		return true;
	}
	else return false;
}
bool CollisionManager::CheckCollisionCircleBox(sf::RectangleShape xCollider1, sf::CircleShape xCollider2)
{
	if (xCollider1.getGlobalBounds().left < xCollider2.getGlobalBounds().left + xCollider2.getGlobalBounds().width &&
		xCollider1.getGlobalBounds().left + xCollider1.getGlobalBounds().width > xCollider2.getGlobalBounds().left &&
		xCollider1.getGlobalBounds().top < xCollider2.getGlobalBounds().top + xCollider2.getGlobalBounds().height &&
		xCollider1.getGlobalBounds().top + xCollider1.getGlobalBounds().height > xCollider2.getGlobalBounds().top)
	{
		return true;
	}
	else return false;
}
///This code is translated from Simple Collision Detection for SFML 2 by ahnonay
OrientedBoundingBox::OrientedBoundingBox(const sf::RectangleShape Object)
{
	sf::Transform trans = Object.getTransform();
	sf::FloatRect local = Object.getLocalBounds();
	Points[0] = trans.transformPoint(0.f, 0.f);
	Points[1] = trans.transformPoint(local.width, 0.f);
	Points[2] = trans.transformPoint(local.width, local.height);
	Points[3] = trans.transformPoint(0.f, local.height);
}

void OrientedBoundingBox::ProjectOntoAxis(const sf::Vector2f& Axis, float& Min, float& Max)
{
	Min = (Points[0].x*Axis.x + Points[0].y*Axis.y);
	Max = Min;
	for (int j = 1; j<4; j++)
	{
		float Projection = (Points[j].x*Axis.x + Points[j].y*Axis.y);

		if (Projection<Min)	
			Min = Projection;
		if (Projection>Max)
			Max = Projection;
	}
}

bool CollisionManager::CheckCollisionRRectangle(sf::RectangleShape xCollider1, sf::RectangleShape xCollider2)
{
	OrientedBoundingBox OBB1(xCollider1);
	OrientedBoundingBox OBB2(xCollider2);

	sf::Vector2f Axes[4] = {
		sf::Vector2f(OBB1.Points[1].x - OBB1.Points[0].x,
		OBB1.Points[1].y - OBB1.Points[0].y),
		sf::Vector2f(OBB1.Points[1].x - OBB1.Points[2].x,
			OBB1.Points[1].y - OBB1.Points[2].y),
		sf::Vector2f(OBB2.Points[0].x - OBB2.Points[3].x,
			OBB2.Points[0].y - OBB2.Points[3].y),
		sf::Vector2f(OBB2.Points[0].x - OBB2.Points[1].x,
			OBB2.Points[0].y - OBB2.Points[1].y)
	};

	for (int i = 0; i<4; i++)
	{
		float MinOBB1, MaxOBB1, MinOBB2, MaxOBB2;

		OBB1.ProjectOntoAxis(Axes[i], MinOBB1, MaxOBB1);
		OBB2.ProjectOntoAxis(Axes[i], MinOBB2, MaxOBB2);

		if (!((MinOBB2 <= MaxOBB1) && (MaxOBB2 >= MinOBB1)))
		{
			return false;
		}
	}
	return true;
	
}
bool CollisionManager::CheckCollisionPoint()
{
	return false;
}



