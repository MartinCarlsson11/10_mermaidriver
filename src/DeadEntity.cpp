#include "stdafx.h"
#include "DeadEntity.h"
#include "ParticleHandler.h"
DeadEntity::DeadEntity(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	mParticleHandler = nullptr;
	CreateSprite();
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
	if (mType == Type_Swordfish)
	{
		mParticleHandler = new ParticleHandler(mSystem, 10, mPosition, sf::Color::Red);
	}
}
DeadEntity::~DeadEntity()
{
	if (mParticleHandler != nullptr)
	{
		delete mParticleHandler;
		mParticleHandler = nullptr;
	}
	mAnimationFrames.~AnimateSprite();
}

bool DeadEntity::Update()
{
	mSprite.setPosition(mPosition);
	if (mType == Type_Blowfish || mType == Type_Jellyfish)
	{
		mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));
	}
	if (mParticleHandler != nullptr)
	{
		mParticleHandler->Update(0.16, mPosition, 1);
	}

	Move();

	if (mPosition.x < 0 - 400)
	{
		return false;
	}
	else
	{
		return true;
	}
}

ParticleHandler* DeadEntity::GetParticles()
{
	return mParticleHandler;
}

EntityType DeadEntity::GetType()
{
	return mType;
}

sf::Sprite DeadEntity::GetSprite()
{
	return mSprite;
}
sf::Vector2f DeadEntity::GetPosition()
{
	return mPosition;
}
void DeadEntity::Move()
{
	if (mType == Type_Swordfish)
	{
		mVelocity.y = -2;
		if (mPosition.y < 720)
		{
			mVelocity.y = 0;
		}
		
	}
	mPosition += mVelocity;
}
void DeadEntity::CreateSprite()
{
	if (mType == Type_Blowfish)
	{
		mSprite = mSystem.m_pxSpriteManager->CreateSprite("../Assets/level1/death/Blowfish.png", 0, 0, 362, 330);

		mAnimationFrames.AddFrame(0, 0, 362, 330, 1);
		mAnimationFrames.AddFrame(362, 0, 362, 330, 0.5);
		mAnimationFrames.AddFrame(724, 0, 362, 330, 0.5);
		mAnimationFrames.AddFrame(1086, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(1448, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(1810, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(2172, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(2534, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(2896, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(3258, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(3620, 0, 362, 330, 0.3);
		mAnimationFrames.AddFrame(3982, 0, 362, 330, 1);
		mAnimationFrames.AddFrame(0, 0, 0, 0, 1000);

	}
	if (mType == Type_Jellyfish)
	{
		mSprite = mSystem.m_pxSpriteManager->CreateSprite("../Assets/level1/death/Jellyfish.png", 0, 0, 0, 0);
		mAnimationFrames.AddFrame(0, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(303, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(606, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(909, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(1212, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(1515, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(1818, 0, 303, 273, 1);
		mAnimationFrames.AddFrame(0, 0, 0, 0, 1000);
	}
	if (mType == Type_Swordfish)
	{
		mSprite = mSystem.m_pxSpriteManager->CreateSprite("../Assets/level1/death/Swordfish.png", 0, 0, 720, 373);
		mSprite.setScale(0.4f, 0.4f);
	}
	mSprite.setOrigin(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2);
}


void DeadEntity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}