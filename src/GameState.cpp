#include "stdafx.h"
#include <string>
#include <iostream>
#include "ParticleHandler.h"
#include "GameState.h"
#include "ParallaxHandler.h"
#include "CollisionManager.h"
#include "EndState.h"

GameState::GameState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_xLevel = new Level(m_xSystem.m_pxSpriteManager);
	m_xParallaxHandler = new ParallaxHandler();
	DrawLayers = m_xParallaxHandler->GetLayers();
	GameView.setSize(m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
	HighscoreFont = new sf::Font();
	HighscoreFont->loadFromFile("../assets/font/Lato-Regular.ttf");
	
	m_iFlashDrown = 0;
	mbDrownFlashScreen = true;

	mSpawnPoints = m_xLevel->GetSpawner();
}

GameState::~GameState()
{
	m_xSystem.m_pxSoundManager->StopMusic();

	delete m_pxCaptain;
	m_pxCaptain = nullptr;

	delete m_pxHarpoon;
	m_pxHarpoon = nullptr;

	delete m_xPowerupTimer;
	m_xPowerupTimer = nullptr;

	delete m_xLevel;
	m_xLevel = nullptr;

	delete m_xParallaxHandler;
	m_xParallaxHandler = nullptr;

	DrawLayers.clear();

	for (int i = 0; i < m_pxDeadEntityVector.size(); i++)
	{
		delete m_pxDeadEntityVector[i];
		m_pxDeadEntityVector[i] = nullptr;
	}
	m_pxDeadEntityVector.clear();

	for (int i = 0; i < mProjectiles.size(); i++)
	{
		delete mProjectiles[i];
		mProjectiles[i] = nullptr;
	}
	mProjectiles.clear();

	for (int i = 0; i < mEntityVector.size(); i++)
	{
		delete mEntityVector[i];
		mEntityVector[i] = nullptr;
	}
	mEntityVector.clear();

	delete HighscoreFont;
	HighscoreFont = nullptr;

	delete mParticleHandler;
	delete mLevelParticles;

	mParticleHandler = nullptr;
	mLevelParticles = nullptr;

	WinGame = false;
	ElectricityDamage = false;

	delete HighscoreFont;
	HighscoreFont = nullptr;

	mSpawnPoints.clear();


}

void GameState::Enter()
{
	m_pxHarpoon = new Harpoon(Type_Harpoon, sf::Vector2f(0,0), sf::Vector2f(0,0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem);


	mTutorialMouse1Fade = false;
	 mTutorialMouse0Fade = false;
	 mTutorialWFade = false;
	 mTutorialAFade = false;
	 mTutorialSFade = false;
	 mTutorialDFade = false;

	mTutorialMouse1Fader = 255;
	mTutorialMouse0Fader = 255;
	mTutorialWFader = 255;
	mTutorialAFader = 255;
	mTutorialSFader = 255;
	mTutorialDFader = 255;



	m_xSurfaceSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/surface.png", 0, 0, 0, 0);
	m_xSurfaceSprite.setPosition(sf::Vector2f(0, 700));
	m_xSurfaceAnimation.AddFrame(0,0, 1920, 60,1);
	m_xSurfaceAnimation.AddFrame(0, 60, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 120, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 180, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 240, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 300, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 360, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 420, 1920, 60, 1);
	m_xSurfaceAnimation.AddFrame(0, 480, 1920, 60, 1);
	///Temporary hud
	m_xFeedbackDamage = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/gui/Damagefeedback.png", 0, 0, 1920, 1080);
	m_xFeedbackDamage.setColor(sf::Color(100, 100, 255,0));

	m_xFeedbackDrown = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/gui/drownfeedback.png", 0, 0, 1920, 1080);
	m_xFeedbackDrown.setColor(sf::Color(0, 0, 0, 0));

	m_xHudElementHead = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 0, 240, 240);
	m_xHudElementHPbar = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 1440, 36, 120, 147);
	m_xHudElementAirbar = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 1560,36, 120, 147);
	m_xHudElementAirStatus = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 0, 0, 0);
	m_xHudElementHPbar.setScale(sf::Vector2f(1.0f, -1.0f));
	m_xHudElementAirbar.setScale(sf::Vector2f(1.0f, -1.0f));

	m_xHudElementAirStatus.setPosition(840, 0);
	m_xHudElementHead.setPosition(840, 0);
	m_xHudElementAirbar.setPosition(960, 183);
	m_xHudElementHPbar.setPosition(840, 183);

	m_xHudElementHighscore = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 240, 240, 240);
	m_xHudElementPowerups = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 240, 240, 240, 240);

	HighscoreText.setFont(*HighscoreFont);
	HighscoreText.setColor(sf::Color::White);

	HighscoreText.setCharacterSize(20);
	HighscoreText.setPosition(692, 108);

	m_xHudElementHighscore.setPosition(600, 0);
	m_xHudElementPowerups.setPosition(1080, 0);

	m_xHEPowerupBubble = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 0, 0, 0);
	m_xHEPowerupBubble.setPosition(1160, 100);
	m_xHEPowerupSword = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 0, 0, 0);
	m_xHEPowerupChain = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Gui/hud.png", 0, 0, 0, 0);

	mBubbleFeedback = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/powerups/bubblefeedback.png", 0,0,0,0);

	m_xCursorSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/menu/cursor.png", 0, 0, 0, 0);
	m_xCursorSprite.setTextureRect(sf::IntRect(10, 7, 26, 30));
	mAnimateCursor.AddFrame(10, 7, 26, 30, 0.05);
	mAnimateCursor.AddFrame(50, 7, 26, 30, 0.05);
	mAnimateCursor.AddFrame(90, 7, 26, 30, 0.05);
	mAnimateCursor.AddFrame(130, 7, 26, 30, 0.05);
	mAnimateCursor.AddFrame(170, 7, 26, 30, 0.05);
	mAnimateCursor.AddFrame(10, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(50, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(90, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(130, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(170, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(10, 47, 26, 30, 0.05);
	mAnimateCursor.AddFrame(50, 87, 26, 30, 0.05);
	mAnimateCursor.AddFrame(90, 87, 26, 30, 0.05);
	mAnimateCursor.AddFrame(130, 87, 26, 30, 0.05);
	mAnimateCursor.AddFrame(170, 87, 26, 30, 0.05);
	mAnimateCursor.AddFrame(10, 127, 26, 30, 0.05);
	mAnimateCursor.AddFrame(50, 127, 26, 30, 0.05);
	mAnimateCursor.AddFrame(90, 127, 26, 30, 0.05);
	mAnimateCursor.AddFrame(130, 127, 26, 30, 0.05);
	mAnimateCursor.AddFrame(170, 127, 26, 30, 0.05);
	mAnimateCursor.AddFrame(170, 127, 26, 30, 10000);



	mTutorialMouse1 = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialMouse1.setTextureRect(sf::IntRect(330, 0, 215, 400));

	mTutorialMouse0 = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialMouse0.setTextureRect(sf::IntRect(170, 35, 125, 125));
	mTutorialW = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialW.setTextureRect(sf::IntRect(25, 50, 100, 65));

	mTutorialA = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialA.setTextureRect(sf::IntRect(25, 120, 100, 65));
	mTutorialS = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialS.setTextureRect(sf::IntRect(25, 260, 100, 65));
	mTutorialD = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/controls.png", 0, 0, 0, 0);
	mTutorialD.setTextureRect(sf::IntRect(25, 190, 100, 65));




	mWinChest = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/chest.png", 0, 0, 0, 0);
	mWinChest.setTextureRect(sf::IntRect(0,0, 250, 250));
	mWinChest.setPosition(27820, 1440);
	mParticleHandler = new ParticleHandler(m_xSystem, 9, sf::Vector2f(0, 0), sf::Color::Transparent);
	mLevelParticles = new ParticleHandler(m_xSystem, 5, sf::Vector2f(0, 0), sf::Color::Blue);
	m_xSystem.m_pxSoundManager->PlayMusic(m_xSystem.m_pxSoundManager->Music);

	m_pxCaptain = new Captain(Type_Captain, sf::Vector2f(400, 1000), sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem);


	m_xSystem.m_pxWindow->setMouseCursorVisible(false);

}

bool GameState::Update(float p_fDeltaTime)
{
	if (m_pxCaptain != nullptr)
	{


		mTutorialMouse1.setPosition(m_pxCaptain->GetPosition().x + 500, m_pxCaptain->GetPosition().y - 75);
		mTutorialMouse0.setPosition(m_pxCaptain->GetPosition().x + 500, m_pxCaptain->GetPosition().y);
		mTutorialW.setPosition(m_pxCaptain->GetPosition().x, m_pxCaptain->GetPosition().y - 125);
		mTutorialA.setPosition(m_pxCaptain->GetPosition().x - 125, m_pxCaptain->GetPosition().y );
		mTutorialS.setPosition(m_pxCaptain->GetPosition().x , m_pxCaptain->GetPosition().y + 125);
		mTutorialD.setPosition(m_pxCaptain->GetPosition().x + 125, m_pxCaptain->GetPosition().y);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			mTutorialWFade = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			mTutorialAFade = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			mTutorialSFade = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			mTutorialDFade = true;
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			mTutorialMouse0Fade = true;
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			mTutorialMouse1Fade = true;
		}
		
		if (mTutorialWFade == true)
		{
			mTutorialWFader -= 5;
			mTutorialW.setColor(sf::Color(255, 255, 255, mTutorialWFader));
			if (mTutorialWFader == 0)
			{
				mTutorialW.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		if (mTutorialAFade == true)
		{
			mTutorialAFader -= 5;;
			mTutorialA.setColor(sf::Color(255, 255, 255, mTutorialAFader));
			if (mTutorialAFader == 0)
			{
				mTutorialA.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		if (mTutorialSFade == true)
		{
			mTutorialSFader -= 5;
			mTutorialS.setColor(sf::Color(255, 255, 255, mTutorialSFader));
			if (mTutorialSFader == 0)
			{
				mTutorialS.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		if (mTutorialDFade == true)
		{
			mTutorialDFader -= 5;
			mTutorialD.setColor(sf::Color(255, 255, 255, mTutorialDFader));
			if (mTutorialDFader == 0)
			{
				mTutorialD.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		
		if (mTutorialMouse0Fade == true)
		{
			mTutorialMouse0Fader -= 5;
			mTutorialMouse0.setColor(sf::Color(255, 255, 255, mTutorialMouse0Fader));
			if (mTutorialMouse0Fader == 0)
			{
				mTutorialMouse0.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		if (mTutorialMouse1Fade == true)
		{
			mTutorialMouse1Fader -= 5;
			mTutorialMouse1.setColor(sf::Color(255, 255, 255, mTutorialMouse1Fader));
			if (mTutorialMouse1Fader == 0)
			{
				mTutorialMouse1.setTextureRect(sf::IntRect(0, 0, 0, 0));
			}
		}
		mWinChest.move(-m_xParallaxHandler->GetScrollSpeed(8), 0);	
		if (m_pxCaptain->Update(GameView))
		{
			if (m_xPowerupTimer != nullptr)
			{
				m_xPowerupTimer->Update(m_xPowerupClock.getElapsedTime());
				if (m_xPowerupTimer->Check() == 1)
				{
					m_pxCaptain->SetPowerUpActive(false);
				}
			}

			if (m_pxHarpoon->GetWindupTime() > 0)
			{
				m_xCursorSprite.setTextureRect(mAnimateCursor.UpdateASprite(0.008));
			}
			else
			{
				mAnimateCursor.Reset();
				m_xCursorSprite.setTextureRect(sf::IntRect(10, 7, 26, 30));
			}
			if (m_pxHarpoon->GetWindupTime() == 19.f)
			{
				m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->HarpoonFullChargeSound, false);

			}


			if (m_iFlashDamage > 0)
			{
				m_iFlashDamage -= 5;
			}
		
			m_xFeedbackDamage.setColor(sf::Color(100, 100, 255, m_iFlashDamage));

			mParticleHandler->Update(0.016f, sf::Vector2f(m_pxCaptain->GetPosition().x + 75, m_pxCaptain->GetPosition().y - 50), m_xAirSystem.GetAirLevel());
			mLevelParticles->Update(0.016f, sf::Vector2f(2000, 1680), 1);

			m_xAirSystem.Update(*m_pxCaptain);
			m_xSurfaceSprite.setTextureRect(m_xSurfaceAnimation.UpdateASprite(0.16));
			if (m_xAirSystem.GetAirLevel() == 0)
			{
				m_iFlashDamage = 255;
				m_pxCaptain->TakeDamage(1, false);
			}

			//Temporary HUD
			{
				if (m_xAirSystem.GetAirLevel() < 10)
				{

					if (m_iFlashDrown == 255)
					{
						mbDrownFlashScreen = true;

					}
					if (m_iFlashDrown == 0)
					{
						mbDrownFlashScreen = false;
					}

					if (mbDrownFlashScreen == true)
					{
						m_iFlashDrown--;
						m_iFlashDrown--;
						m_iFlashDrown--;
						m_iFlashDrown--;
						m_iFlashDrown--;
					}
					else
					{
						m_iFlashDrown++;
						m_iFlashDrown++;
						m_iFlashDrown++;
						m_iFlashDrown++;
						m_iFlashDrown++;
					}

					m_xFeedbackDrown.setColor(sf::Color(0, 64, 255, m_iFlashDrown));
				}
				else
				{
					m_xFeedbackDrown.setColor(sf::Color(0, 64, 255, 0));
				}


				HighscoreText.setString(std::to_string(m_xHighScore.GetHighscore()));
				if (m_pxCaptain->GetHP() == 4)
				{
					m_xHudElementHPbar.setTextureRect(sf::IntRect(1440, 35, 120, ((147 / 4) * m_pxCaptain->GetHP())));
					m_xHudElementHead.setTextureRect(sf::IntRect(0, 0, 240, 240));
				}
				else if (m_pxCaptain->GetHP() == 3)
				{
					m_xHudElementHPbar.setTextureRect(sf::IntRect(1440, 35, 120, ((147 / 4) * m_pxCaptain->GetHP())));
					m_xHudElementHead.setTextureRect(sf::IntRect(0, 0, 240, 240));
				}
				else if (m_pxCaptain->GetHP() == 2)
				{
					m_xHudElementHPbar.setTextureRect(sf::IntRect(1440, 35, 120, ((147 / 4) * m_pxCaptain->GetHP())));
					m_xHudElementHead.setTextureRect(sf::IntRect(240, 0, 240, 240));
				}
				else if (m_pxCaptain->GetHP() == 1)
				{
					m_xHudElementHPbar.setTextureRect(sf::IntRect(1440, 35, 120, ((147 / 4) * m_pxCaptain->GetHP())));
					m_xHudElementHead.setTextureRect(sf::IntRect(480, 0, 240, 240));
				}
				m_xHudElementAirbar.setTextureRect(sf::IntRect(1560, 35, 120, 5 * m_xAirSystem.GetAirLevel()));



				if (m_pxCaptain->BubblePowerup())
				{
					m_xHEPowerupBubble.setTextureRect(sf::IntRect(550, 340, 40, 40));
					mBubbleFeedback.setTextureRect(sf::IntRect(0, 0, 1920, 1080));
				}
				else
				{
					m_xHEPowerupBubble.setTextureRect(sf::IntRect(0, 0, 0, 0));
					mBubbleFeedback.setTextureRect(sf::IntRect(0, 0, 0, 0));
				}

				if (m_xAirSystem.GetAirLevel() < 30 / 1.5)
				{
					m_xHudElementAirStatus.setTextureRect(sf::IntRect(720, 0, 240, 240));
				}
				if (m_xAirSystem.GetAirLevel() < 30 / 2)
				{
					m_xHudElementAirStatus.setTextureRect(sf::IntRect(960, 0, 240, 240));
				}
				if (m_xAirSystem.GetAirLevel() < 30 / 4)
				{
					m_xHudElementAirStatus.setTextureRect(sf::IntRect(1200, 0, 240, 240));
				}

				if (m_xAirSystem.GetAirLevel() == 30)
				{
					m_xHudElementAirStatus.setTextureRect(sf::IntRect(0, 0, 0, 0));
				}

			}



			for (auto it = mSpawnPoints.begin(); it != mSpawnPoints.end(); )
			{
				(*it).mPos.x -= m_xParallaxHandler->GetScrollSpeed(8);

				if ((*it).mPos.x < 2500)
				{
					if ((*it).mType == Type_Bird)
					{
						mEntityVector.push_back(new Bird(Type_Bird, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Blowfish)
					{
						mEntityVector.push_back(new Blowfish(Type_Blowfish, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Jellyfish)
					{
						mEntityVector.push_back(new Jellyfish(Type_Jellyfish, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Swordfish)
					{
						mEntityVector.push_back(new Swordfish(Type_Swordfish, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Coin)
					{
						mEntityVector.push_back(new Coins(Type_Coin, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Rum)
					{
						mEntityVector.push_back(new Rum(Type_Rum, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Flyfish)
					{
						mEntityVector.push_back(new Flyfish(Type_Flyfish, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}
					if ((*it).mType == Type_Powerup)
					{
						mEntityVector.push_back(new PowerUp(Type_Powerup, it->mPos, sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(8), m_xSystem));
					}

					mSpawnPoints.erase(it);
					it = mSpawnPoints.end();
				}
				else
				{
					++it;
				}
			}

			if (m_pxHarpoon != nullptr)
			{
				m_pxHarpoon->Update(GameView, *m_pxCaptain, ElectricityDamage);
				ElectricityDamage = false;
			}

			auto EntityVector = mEntityVector.begin();
			while (EntityVector != mEntityVector.end())
			{
				if (!(*EntityVector)->Update())
				{
					delete *EntityVector;
					*EntityVector = nullptr;
					mEntityVector.erase(EntityVector);
					
					EntityVector = mEntityVector.end();
				}
				else
				{
					EntityVector++;
				}
			}


			for (auto it = mProjectiles.begin(); it != mProjectiles.end();)
			{
				if ((*it)->Update() == false)
				{
					delete *it;
					*it = nullptr;

					mProjectiles.erase(it);
					it = mProjectiles.end();
				}
				else
				{
					++it;
				}
			}

			for (auto it = m_pxDeadEntityVector.begin(); it != m_pxDeadEntityVector.end();)
			{
				if ((*it)->Update() == false)
				{
					delete *it;
					*it = nullptr;
					it = m_pxDeadEntityVector.erase(it);
				}
				else
				{
					++it;
				}
			}

			//Collision Checking
			if (m_pxHarpoon != nullptr)
			{
				if (m_pxHarpoon->HarpoonState() == false)
				{
					if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), m_pxHarpoon->GetColliderRect1()))
					{
						m_pxHarpoon->HarpoonInactive();
					}
				}
			}

			auto ProjectileVector = mProjectiles.begin();
			for (auto ProjectileVector = mProjectiles.begin(); ProjectileVector != mProjectiles.end();)
			{
				if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*ProjectileVector)->GetColliderRect1()))
				{
					m_iFlashDamage = 255;
					m_pxCaptain->TakeDamage(1, false);
					delete (*ProjectileVector);
					(*ProjectileVector) = nullptr;
				}
				if ((*ProjectileVector) == nullptr)
				{
					mProjectiles.erase(ProjectileVector);
					ProjectileVector = mProjectiles.end();
				}
				else
				{
					ProjectileVector++;
				}
			}

			auto CollisionEntities = mEntityVector.begin();
			while (CollisionEntities != mEntityVector.end())
			{
				if ((*CollisionEntities)->GetPosition().x < 1920)
				{
					if ((*CollisionEntities)->GetType() == Type_Bird)
					{
						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect1()))
						{
							m_iFlashDamage = 255;
							m_pxCaptain->TakeDamage(1, false);
						}

					}
					else if ((*CollisionEntities)->GetType() == Type_Blowfish)
					{
						if ((*CollisionEntities)->GetPosition().x < 1920)
						{
							if (CollisionManager::CheckCollisionAABB((*CollisionEntities)->GetColliderRect1(), m_pxCaptain->GetColliderRect1()) || CollisionManager::CheckCollisionRRectangle((*CollisionEntities)->GetColliderRect1(), m_pxHarpoon->GetColliderRect2()))
							{
								m_pxDeadEntityVector.push_back(new DeadEntity(Type_Blowfish, (*CollisionEntities)->GetPosition(), sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));

								int Angle = 360;
								sf::Vector2f Velocity;
								for (int j = 0; j < 11; j++)
								{
									Velocity.x = cos(Angle) * 5;
									Velocity.y = sin(Angle) * 5;
									mProjectiles.push_back(new Projectiles(Type_Projectiles, (*CollisionEntities)->GetPosition(), Velocity, m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));
									Angle -= 36;
								}

								delete *CollisionEntities;
								*CollisionEntities = nullptr;
								m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->BlowfishdeathSound, true);
							}

							for (auto it = mProjectiles.begin(); it != mProjectiles.end();)
							{
								if ((*CollisionEntities) != nullptr)
								{
									if (CollisionManager::CheckCollisionAABB((*CollisionEntities)->GetColliderRect1(), (*it)->GetColliderRect1()))
									{
										m_pxDeadEntityVector.push_back(new DeadEntity(Type_Blowfish, (*CollisionEntities)->GetPosition(), sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));

										int Angle = 360;
										sf::Vector2f Velocity;
										for (int j = 0; j < 11; j++)
										{
											Velocity.x = cos(Angle) * 5;
											Velocity.y = sin(Angle) * 5;
											mProjectiles.push_back(new Projectiles(Type_Projectiles, (*CollisionEntities)->GetPosition(), Velocity, m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));
											Angle -= 36;
										}

										delete *CollisionEntities;
										*CollisionEntities = nullptr;
										m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->BlowfishdeathSound, true);
										it = mProjectiles.end();
									}
									else
									{
										++it;
									}

								}
								else
								{
									++it;
								}

							}
						}
						
					}
					else if ((*CollisionEntities)->GetType() == Type_Coin)
					{
						if (CollisionManager::CheckCollisionAABB((*CollisionEntities)->GetColliderRect1(), m_pxCaptain->GetColliderRect1()))
						{
							delete *CollisionEntities;
							*CollisionEntities = nullptr;
							m_xHighScore.AddHighscore(5);
							m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->CoinPickupSound, true);
						}
					}
					else if ((*CollisionEntities)->GetType() == Type_Flyfish)
					{
						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect1()))
						{
							m_iFlashDamage = 255;
							m_pxCaptain->TakeDamage(1, false);
						}
					}
					else if ((*CollisionEntities)->GetType() == Type_Jellyfish)
					{

						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect1()))
						{
							m_iFlashDamage = 255;
							m_pxCaptain->TakeDamage(1, true);
						}
						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect2()))
						{
							m_iFlashDamage = 255;
							m_pxCaptain->TakeDamage(1, true);
						}
						sf::FloatRect xRect = sf::FloatRect((*CollisionEntities)->GetPosition().x, (*CollisionEntities)->GetPosition().y, 500.f, 500.f);
						if (xRect.intersects(m_pxHarpoon->GetColliderRect1().getGlobalBounds()))
						{
							if (CollisionManager::CheckCollisionRRectangle(m_pxHarpoon->GetColliderRect2(), (*CollisionEntities)->GetColliderRect1()))
							{
								m_pxDeadEntityVector.push_back(new DeadEntity(Type_Jellyfish, (*CollisionEntities)->GetPosition(), sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));

								delete *CollisionEntities;
								*CollisionEntities = nullptr;
								m_xHighScore.AddHighscore(4, 1);
								m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->JellyfishDeathSound, true);
							}
							else if (CollisionManager::CheckCollisionRRectangle((*CollisionEntities)->GetColliderRect2(), m_pxHarpoon->GetColliderRect1()))
							{
								m_pxCaptain->TakeDamage(1, true);
								ElectricityDamage = true;
								m_iFlashDamage = 255;
							}
						}
					}
					else if ((*CollisionEntities)->GetType() == Type_Powerup)
					{
						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect1()))
						{
							m_pxCaptain->SetPowerUpActive(true);
							m_xPowerupClock.restart();
							m_xPowerupTimer = new SimpleTimer(20000, m_xPowerupClock.getElapsedTime());

							delete *CollisionEntities;
							*CollisionEntities = nullptr;
						}
					}
					else if ((*CollisionEntities)->GetType() == Type_Rum)
					{
						if (CollisionManager::CheckCollisionAABB(m_pxCaptain->GetColliderRect1(), (*CollisionEntities)->GetColliderRect1()))
						{
							delete *CollisionEntities;
							*CollisionEntities = nullptr;
							m_pxCaptain->RestoreHealth(1);
						}
					}
					else if ((*CollisionEntities)->GetType() == Type_Swordfish)
					{
						if (CollisionManager::CheckCollisionAABB((*CollisionEntities)->GetColliderRect1(), m_pxCaptain->GetColliderRect1()))
						{
							m_iFlashDamage = 255;
							m_pxCaptain->TakeDamage(1, false);
						}
						sf::FloatRect xRect = sf::FloatRect((*CollisionEntities)->GetPosition().x, (*CollisionEntities)->GetPosition().y, 500.f, 500.f);
						if (xRect.intersects(m_pxHarpoon->GetColliderRect1().getGlobalBounds()))
						{
							if (CollisionManager::CheckCollisionRRectangle((*CollisionEntities)->GetColliderRect1(), m_pxHarpoon->GetColliderRect1()))
							{
								if (m_pxHarpoon->HarpoonState())
								{

									m_pxDeadEntityVector.push_back(new DeadEntity(Type_Swordfish, sf::Vector2f((*CollisionEntities)->GetColliderRect1().getGlobalBounds().left, (*CollisionEntities)->GetColliderRect1().getGlobalBounds().top), sf::Vector2f(0, 0), m_xParallaxHandler->GetScrollSpeed(6), m_xSystem));

									delete *CollisionEntities;
									*CollisionEntities = nullptr;


									m_xHighScore.AddHighscore(2, 1);
									m_xSystem.m_pxSoundManager->PlaySound(m_xSystem.m_pxSoundManager->SwordfishDeathSound, true);
								}
							}
						}

					}
					if ((*CollisionEntities) == nullptr)
					{
						mEntityVector.erase(CollisionEntities);
						CollisionEntities = mEntityVector.end();
					}
					else
					{
						++CollisionEntities;
					}
				}
				else
				{
					++CollisionEntities;
				}

			}
			if (CollisionManager::CheckCollisionAABB(mWinChest, m_pxCaptain->GetColliderRect1()))
			{
				WinGame = true;
			}

			//debug hitboxes
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
			{
				DebugHitboxes = true;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
			{
				DebugHitboxes = false;
			}
			//restart
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F3))
			{
				return false;
			}
			//100 health
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
			{
				m_pxCaptain->RestoreHealth(100);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
			{
				WinGame = true;
			}
			m_xParallaxHandler->ScrollLayers(m_pxCaptain->GetPosition().y);
			GameView.setCenter(m_xSystem.m_iScreenWidth / 2, m_pxCaptain->GetPosition().y);
			if (GameView.getCenter().y + (GameView.getSize().y *0.5) >= 1680)
			{
				int NewYPos = 1680 - (GameView.getSize().y *0.5);
				GameView.setCenter(m_xSystem.m_iScreenWidth * 0.5, NewYPos);
			}
			else if (GameView.getCenter().y - (GameView.getSize().y *0.5) <= 0)
			{
				int NewYPos = GameView.getSize().y *0.5;
				GameView.setCenter(m_xSystem.m_iScreenWidth * 0.5, NewYPos);
			}

			if (WinGame == true)
			{
				//Animation

				return false;
			}

			

			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
	
}

void GameState::Draw()
{
	if (m_pxCaptain != nullptr)
	{

		//bakground layer
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[0]);

		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		//bakground objects
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[1]);

		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();

		//Parallax 3 layer
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[2]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		//Parallax 3  objects
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[3]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();

		//Parallax 2 layer
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[4]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		//Parallax 2 objects
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[5]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();

		//Parallax 1 layer
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[6]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		//Parallax 1 objects
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[7]);
		m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
		m_xLevel->NextLayer();
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[0]);
		//m_xSystem.m_pxDrawManager->Draw(*mLevelParticles);


		//Gameplay layer
		m_xSystem.m_pxDrawManager->Draw(mWinChest);
		for (auto&& mEntityVector : mEntityVector)
		{
			if (DebugHitboxes == true)
			{
				m_xSystem.m_pxDrawManager->Draw(mEntityVector->GetColliderRect1());
				m_xSystem.m_pxDrawManager->Draw(mEntityVector->GetColliderRect2());
			}
			m_xSystem.m_pxDrawManager->Draw(*mEntityVector);
		}

		
		for (auto&& m_pxDeadEntityVector : m_pxDeadEntityVector)
		{
			if (m_pxDeadEntityVector->GetParticles() != nullptr)
			{
				m_xSystem.m_pxDrawManager->Draw(*m_pxDeadEntityVector->GetParticles());
			}
			m_xSystem.m_pxDrawManager->Draw(m_pxDeadEntityVector->GetSprite(), m_pxDeadEntityVector->GetPosition());
		}

		for (auto && mProjectiles : mProjectiles)
		{
			if (mProjectiles != nullptr)
			{
				m_xSystem.m_pxDrawManager->Draw(*mProjectiles);
			}

		}

		if (DebugHitboxes == true)
		{
			m_xSystem.m_pxDrawManager->Draw(m_pxCaptain->GetColliderRect1());
		}

		m_xSystem.m_pxWindow->setView(GameView);
		if (m_pxHarpoon != nullptr)
		{
			if (DebugHitboxes == true)
			{
				m_xSystem.m_pxDrawManager->Draw(m_pxHarpoon->GetColliderRect1());
				m_xSystem.m_pxDrawManager->Draw(m_pxHarpoon->GetColliderRect2());
			}
			if (m_pxHarpoon->IsVisible())
			{
				m_xSystem.m_pxDrawManager->Draw(*m_pxHarpoon);
				m_xSystem.m_pxDrawManager->Draw(m_pxHarpoon->HarpoonRope());
			}
		}
		m_xSystem.m_pxDrawManager->Draw(*m_pxCaptain);
		m_xSystem.m_pxDrawManager->Draw(*mParticleHandler);
		m_xSystem.m_pxDrawManager->Draw(mTutorialMouse1);
		m_xSystem.m_pxDrawManager->Draw(mTutorialMouse0);
		m_xSystem.m_pxDrawManager->Draw(mTutorialW);
		m_xSystem.m_pxDrawManager->Draw(mTutorialA);
		m_xSystem.m_pxDrawManager->Draw(mTutorialS);
		m_xSystem.m_pxDrawManager->Draw(mTutorialD);
		
	
		m_xSystem.m_pxDrawManager->SetView(*DrawLayers[0]);

		m_xSystem.m_pxDrawManager->Draw(m_xSurfaceSprite);
	}


	//foreground layer
	m_xSystem.m_pxDrawManager->SetView(*DrawLayers[8]);
	m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
	m_xLevel->NextLayer();
	m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
	m_xLevel->NextLayer();
	//Foreground objects
	m_xSystem.m_pxDrawManager->SetView(*DrawLayers[9]);
	m_xSystem.m_pxDrawManager->Draw(*m_xLevel);
	m_xLevel->NextLayer();


	sf::View HudView;
	HudView.reset(sf::FloatRect(0, 0, 1920, 1080));
	m_xSystem.m_pxDrawManager->SetView(HudView);
	m_xSystem.m_pxDrawManager->Draw(mBubbleFeedback);
	m_xSystem.m_pxDrawManager->Draw(m_xFeedbackDamage);
	m_xSystem.m_pxDrawManager->Draw(m_xFeedbackDrown);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementAirbar);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementHPbar);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementHead);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementAirStatus);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementHighscore);
	m_xSystem.m_pxDrawManager->Draw(m_xHudElementPowerups);
	m_xSystem.m_pxDrawManager->Draw(HighscoreText);
	m_xSystem.m_pxDrawManager->Draw(m_xHEPowerupBubble);

	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite, sf::Vector2f(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y));
}
	

void GameState::Exit()
{
}

IState* GameState::NextState()
{
	return new EndState(m_xSystem, WinGame, m_xHighScore);
}