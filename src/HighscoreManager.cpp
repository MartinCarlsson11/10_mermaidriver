#include "HighscoreManager.h"
HighscoreManager::HighscoreManager()
{
}

HighscoreManager::~HighscoreManager()
{
}

int HighscoreManager::GetHighscore()
{
	return Highscore;
}

int HighscoreManager::GetBonus()
{
	return Bonus;
}

int HighscoreManager::GetNewPoints()
{
	return NewPoints;
}

void HighscoreManager::AddHighscore(int points, int bonus)
{
	Highscore += points;
}

void HighscoreManager::AddHighscore(int points)
{
	Highscore += points;
}

void HighscoreManager::ResetBonus()
{
	Bonus = 0;
}

void HighscoreManager::UpdateHighscore()
{
	Highscore += (NewPoints * Bonus);
	ResetBonus();
}

