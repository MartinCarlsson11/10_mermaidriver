#include "stdafx.h"
#include "ParticleHandler.h"
#include "Particle.h"

ParticleHandler::ParticleHandler(System xSystem, int xMaxNum, sf::Vector2f xLocation, sf::Color xColor)
	
{
	RandomColor = xColor;
	mActiveParticles = 0;
	mMaxParticleNum = xMaxNum;
	mEmitterLoc = xLocation;
	mSystem = xSystem;
}
ParticleHandler::~ParticleHandler()
{
	auto it = mParticles.begin();
	while (it != mParticles.end())
	{
		delete *it;
		*it = nullptr;

		it++;
	}
}

void ParticleHandler::Update(float deltatime, sf::Vector2f xEmitPos, float xMagnitude)
{
	mEmitterLoc = xEmitPos;
	mFrequency = xMagnitude;
	for (; mActiveParticles < mMaxParticleNum; mActiveParticles++)
	{
		mParticles.push_back(GenerateNewParticle());
	}
	for (int i = 0; i <  mParticles.size(); i++)
	{
		mParticles[i]->Update();
		if (mParticles[i]->TTL <= 0)
		{
			delete mParticles[i];
			mParticles.erase(mParticles.begin() + i);
			mActiveParticles--;
		}
	}
}
void ParticleHandler::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (auto&& mParticles : mParticles)
	{
		mParticles->draw(target, states);
	}
}

Particle* ParticleHandler::GenerateNewParticle()
{
	std::uniform_real_distribution<float> distribution(1.f, 6.f);
	std::uniform_real_distribution<float> distributionSize(1.5f, 2.5f);
	float Randomizer = distribution(generator);
	float Randomizer2 = distribution(generator);
	float RandomizerSize = distributionSize(generator);
	
	sf::Vector2f RandomVel = sf::Vector2f(Randomizer / 6.f, Randomizer2 / 6.f);
	sf::Vector2f RandomSize = sf::Vector2f(RandomizerSize* (2.0f/((int)mFrequency + 5)), RandomizerSize* (2.0f / ((int)mFrequency +5)));
	
	float RandomAngle = Randomizer / (3.14/90);
	float RandomAngleVel = 0.f;
	int RandomTTL = (300 / Randomizer2)*2;

	Particle* newParticle;
	newParticle = new Particle(mSystem, mEmitterLoc, RandomVel, RandomSize, RandomColor, RandomAngle, RandomAngleVel, RandomTTL);

	return newParticle;
}

void ParticleHandler::RemoveParticle()
{

}