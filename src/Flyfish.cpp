#include "stdafx.h"
#include "Flyfish.h"
Flyfish::Flyfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{
	mVelocity = sf::Vector2f(xScrollSpeed, 0.f);
	CreateSprite();

	//Set position 720
}

Flyfish::~Flyfish()
{
	mAnimationFrames.~AnimateSprite();
}

bool Flyfish::Update()
{
	
	mSprite.setPosition(mPosition);
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(40.f, 35.f));
	xCollider.setPosition(sf::Vector2f(mPosition.x, mPosition.y + 40.f));

	SetColliderRect1(xCollider);

	mSoundTime += mSoundClock.restart();
	if (mSoundTime.asSeconds() > 1.5f)
	{
		mSystem.m_pxSoundManager->PlaySound(mSystem.m_pxSoundManager->FlyfishswimSound, true);
	}


	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));

	Move();
	if (mPosition.x < 0 - 120)
	{
		return false;
	}
	else
	return true;
}

void Flyfish::Move()
{
	mPosition += mVelocity;
}

sf::Vector2f Flyfish::GetPosition()
{
	return mPosition;
}
sf::Sprite Flyfish::GetSprite()
{
	return mSprite;
}

EntityType Flyfish::GetType()
{
	return mType;
}


void Flyfish::CreateSprite()
{
	mAnimationFrames.AddFramesFromFile(mSystem.m_pxSpriteManager->GetAnimation("../assets/level1/flyfish/animation/idle.txt"));
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/flyfish/flyfish.PNG", 0, 0, 0, 0);

}
void Flyfish::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}