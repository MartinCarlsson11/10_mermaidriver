#include "stdafx.h"
#include "Blowfish.h"

Blowfish::Blowfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	CreateSprite();
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
}

Blowfish::~Blowfish()
{
	mAnimationFrames.~AnimateSprite();
}

bool Blowfish::Update()
{
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(80.f, 50.f));
	xCollider.setPosition(sf::Vector2f(mPosition.x, mPosition.y + 10));

	SetColliderRect1(xCollider);
	mSprite.setPosition(mPosition);
	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));

	Move();
	if (mPosition.x < 0 - 120)
	{
		return false;
	}
	else
		return true;
}

void Blowfish::Move()
{
	mPosition.x += mVelocity.x;
}

sf::Vector2f Blowfish::GetPosition()
{
	return mPosition;
}
sf::Sprite Blowfish::GetSprite()
{
	return mSprite;
}

void Blowfish::CreateSprite()
{
	
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/blowfish/blowfish.png", 0.f, 0.f, 0, 0);
	mAnimationFrames.AddFramesFromFile(mSystem.m_pxSpriteManager->GetAnimation("../assets/level1/blowfish/animation/idle.txt"));
}

EntityType Blowfish::GetType()
{
	return mType;
}

void Blowfish::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}
