#include "stdafx.h"
#include "Projectiles.h"

Projectiles::Projectiles(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)
{
	CreateSprite();

	mVelocity = sf::Vector2f(xVelocity.x*1, xVelocity.y*1);
	mSprite.setRotation((atan2(xVelocity.y, xVelocity.x) *(180 / 3.14 ) +90));
}

Projectiles::~Projectiles()
{
	mAnimationFrames.~AnimateSprite();
}

sf::Sprite Projectiles::GetSprite()
{
	return mSprite;
}
sf::Vector2f Projectiles::GetPosition()
{
	return mPosition;
}
void Projectiles::Move()
{
	mPosition += mVelocity;
}
void Projectiles::CreateSprite()
{
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/level1/projectile.png", 0, 0, 11, 24);
}

bool Projectiles::Update()
{
	sf::RectangleShape xCollider;
	xCollider.setSize(sf::Vector2f(11.f, 11.f));
	xCollider.setPosition(mPosition);
 	SetColliderRect1(xCollider);
	mSprite.setPosition(mPosition);
	
	if (mPosition.y < 720)
	{
		mVelocity.y = 0;
		mSprite.setRotation(270);
		mVelocity.x = -mScrollSpeed;
	}
	Move();

	if (mPosition.x < 0 || mPosition.x > 1920 || mPosition.y < 0 || mPosition.y > 1680)
	{
		return false;
	}
	return true;
}

void Projectiles::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();
	target.draw(mSprite, states);
}

EntityType  Projectiles::GetType()
{
	return mType;
}
