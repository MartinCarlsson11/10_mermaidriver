#include "stdafx.h"
#include <iostream>
#include <cstring>
#include <math.h>
#include "Level.h"
#include "SpriteManager.h"


Level::Level(SpriteManager* xSpriteManager)
{
	VertexArrayCount = 1;
	this->LoadTiles(xSpriteManager);
	LoadSpawner();
}

Level::~Level()
{
	for (auto&& m_vertices : m_vertices)
	{
		m_vertices.clear();
		m_vertices.resize(0);
	}
	m_vertices.clear();
	m_vertices.resize(0);

	delete mTexture;
	mTexture = nullptr;
}

int Level::GetLayers()
{
	return m_vertices.size();
}

bool Level::LoadTiles(SpriteManager* xSpriteManager)
{
	TiXmlDocument doc("../assets/Tiles.tmx");
	if (!doc.LoadFile())
	{
		std::cerr << doc.ErrorDesc() << std::endl;
	}
	root = doc.FirstChildElement();
	if (root == nullptr)
	{
		std::cerr << "Failed To load File:: No root element." << std::endl;
		doc.Clear();
	}
	int tileWidth = 0;
	int tileHeight = 0;
	int tilecount = 0;

	int width = 0;
	int height = 0;

	int gridWidth = 0;
	int gridHeight = 0;
	int gridOffsetX = 0;
	int gridOffsetY = 0;

	int gid = 0;
	int xLayers = 0;
	int yPos = 0;
	mTexture = new sf::Texture();
	mTexture->loadFromFile("../assets/Level1/Background/Tileset.png");
	mTexture->setSmooth(false);
	for (elem = root->FirstChildElement(); elem != nullptr; elem = elem->NextSiblingElement())
	{
		std::string elemName = elem->Value();
		if (elemName == "tileset")
		{
			tileWidth = std::stoi(elem->Attribute("tilewidth"));
			tileHeight = std::stoi(elem->Attribute("tileheight"));
			tilecount = std::stoi(elem->Attribute("tilecount"));
			

			textureElem = elem->FirstChildElement();
			width = std::stoi(textureElem->Attribute("width"));
			height = std::stoi(textureElem->Attribute("height"));

		}

		if (elemName == "layer")
		{

			gridWidth = std::stoi(elem->Attribute("width"));
			gridHeight = std::stoi(elem->Attribute("height"));
			if (elem->Attribute("offsetx") != nullptr)
			{
				gridOffsetX = std::stoi(elem->Attribute("offsetx"));
			}
			else
			{
				gridOffsetX = 0;
			}

			if (elem->Attribute("offsety") != nullptr)
			{
				gridOffsetY = std::stoi(elem->Attribute("offsety"));
			}
			else
			{
				gridOffsetY = 0;
			}
			m_vertices.resize(xLayers + 1);

			m_vertices[xLayers].setPrimitiveType(sf::Quads);
			m_vertices[xLayers].resize(VertexArrayCount * 4);

			for (dataElem = elem->FirstChildElement(); dataElem != nullptr; dataElem = dataElem->NextSiblingElement())
			{

				elemName = dataElem->Value();
				if (elemName == "data")
				{
					int i = 0;
					for (gidElem = dataElem->FirstChildElement(); gidElem != nullptr; gidElem = gidElem->NextSiblingElement())
					{
						elemName = gidElem->Value();
						if (elemName == "tile")
						{
							gid = std::stoi(gidElem->Attribute("gid"));

							int tileNumber = gid - 1;
							if (i == gridWidth - 1)
							{
								i = 0;
								yPos++;
							}
							else if (yPos != gridHeight)
							{
								if (gid != 0)
								{
									VertexArrayCount++;
									m_vertices[xLayers].resize(VertexArrayCount * 4);

									int tu = tileNumber % (width / tileWidth);
									int tv = tileNumber / (width / tileWidth);

									// get a pointer to the current tile's quad
									sf::Vertex* quad = &m_vertices[xLayers][(VertexArrayCount - 1) * 4];

									// define its 4 corners
									quad[0].position = sf::Vector2f(i * tileWidth + gridOffsetX, yPos * tileHeight + gridOffsetY);
									quad[1].position = sf::Vector2f((i + 1) * tileWidth + gridOffsetX, yPos *  tileHeight + gridOffsetY);
									quad[2].position = sf::Vector2f((i + 1) * tileWidth + gridOffsetX, (yPos + 1) * tileHeight + gridOffsetY);
									quad[3].position = sf::Vector2f(i * tileWidth + gridOffsetX, (yPos + 1) * tileHeight + gridOffsetY);

									// define its 4 texture coordinates
									quad[0].texCoords = sf::Vector2f(tu * tileWidth, tv * tileHeight);
									quad[1].texCoords = sf::Vector2f((tu + 1) * tileWidth, tv *  tileHeight);
									quad[2].texCoords = sf::Vector2f((tu + 1) * tileWidth, (tv + 1) * tileHeight);
									quad[3].texCoords = sf::Vector2f(tu * tileWidth, (tv + 1) * tileHeight);
								}
								i++;
							}
						}

					}

				}
				yPos = 0;
			}
			
			LayerCount++;
			xLayers++;
			VertexArrayCount = 0;

		}
	}
	delete elem;
	elem = nullptr;
	return true;
}

void Level::NextLayer()
{
		CurrentLayer++;

		if (CurrentLayer == m_vertices.size())
		{
			CurrentLayer = 0;
		}
}



void Level::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// apply the transform
	states.transform *= getTransform();

	// apply the tileset texture
	states.texture = mTexture;

	// draw the vertex array
	target.draw(m_vertices[CurrentLayer], states);
}


void Level::MoveSpawner(int ScrollSpeed)
{
	for (auto&& m_SpawnLocations : m_SpawnLocations)
	{
		m_SpawnLocations.mPos.x -= ScrollSpeed;
	}
}
bool Level::LoadSpawner()
{
	//wave one
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(3000.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	//wave two
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(4000.f, 840.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(5250.f, 1060.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(4000.f, 1140.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(4000.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//wave three
	m_SpawnLocations.push_back(SpawnPoint(Type_Rum, sf::Vector2f(5900.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Powerup, sf::Vector2f(5500.f, 1700.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(6100.f, 1400.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(6900.f, 1300.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(6900.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//wave four
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(6100.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(6300.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(7800.f, 940.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(8500.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(7400.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//Wave five
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(8900.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(9000.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(9000.f, 940.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(9100.f, 1140.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(9400.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(9000.f, 1400.f),sf::Vector2f(1.f, 1.f)));
	//wave six
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(11000.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(10500.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(10500.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(10300.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(10720.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(10255.f, 1000.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(10575.f, 1200.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(10500.f, 1400.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(9800.f, 940.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(9850.f, 1100.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(9825.f, 1320.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Rum, sf::Vector2f(9825.f, 1560.f), sf::Vector2f(1.f, 1.f)));

	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(11550.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//Wave eight
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(11500.f, 1500.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(12000.f, 1500.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(12200.f, 1500.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(11900.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12152.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(11335.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(11123.f, 940.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(11525.f, 1040.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(11320.f, 1140.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(12000.f, 940.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(11250.f, 1040.f),sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(11250.f, 1140.f),sf::Vector2f(1.f, 1.f)));
	//wave nine
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(13000.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//Blowfish bullethell formation
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12100.f, 1358), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12420.f, 1234), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12630.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12870.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(12244, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Rum, sf::Vector2f(12244, 1560.f), sf::Vector2f(1.f, 1.f)));
	//Jellyfish protectorate formation
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14211.f, 840.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14300.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14450.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14261.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14440.f, 1240.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(14541.f, 1340.f), sf::Vector2f(1.f, 1.f)));

	//wave ten
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(14261.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(14111.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//swordfish army
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15545.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15775.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15355.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15234.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15630.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(15450.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(15545.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(15450.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//Bird style
	m_SpawnLocations.push_back(SpawnPoint(Type_Powerup, sf::Vector2f(16000.f, 1700.f), sf::Vector2f(1.f, 1.f)));
	//Spawn powerup before each bird style, that way the player has a way to avoid the birds.
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(17000, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(17000.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(17000.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(16300.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(16935, 1560.f), sf::Vector2f(1.f, 1.f)));
	//wave eleven
	//Jellyfish protecting swordfish army4
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18312.f, 780.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18211.f, 920.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18421.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18123.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18251.f, 1240.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18183.f, 1340.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18369.f, 840.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18452.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18352.f, 1110.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18400.f, 1220.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18450.f, 1300.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Jellyfish, sf::Vector2f(18390.f, 1440.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18453.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18345.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18435.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18523.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18324.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(18535.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Rum, sf::Vector2f(19245.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(18452.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(19183.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	//wave twelve
	//triple blowfish bullethell
	//You'll want to dodge these
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21320, 1500.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21130, 1400.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21520, 1240.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(22100, 1340.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21500, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21620, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(22100, 1340.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21500, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21620, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(22100, 1340.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21500, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Blowfish, sf::Vector2f(21620, 1040.f), sf::Vector2f(1.f, 1.f)));

	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(20000, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(21330, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Powerup, sf::Vector2f(22000, 1700.f), sf::Vector2f(1.f, 1.f)));
	//wave Thirteen
	//Bird hell
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(22700, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23300, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23200, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23100, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23050, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23215, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(22755, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(22850, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(22960, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23100, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(22970, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Bird, sf::Vector2f(23315, 1140.f), sf::Vector2f(1.f, 1.f)));

	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(22755, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(22500, 1560.f), sf::Vector2f(1.f, 1.f)));
	//wave fourteen
	//
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(24435, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25363.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26325.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(24000, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25234.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25352.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26768.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26967.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26473.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(24546, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25363.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25467.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25579.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(25786.f, 1140.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26457.f, 940.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Swordfish, sf::Vector2f(26045.f, 1040.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(26500.f, 1560.f), sf::Vector2f(1.f, 1.f)));
	m_SpawnLocations.push_back(SpawnPoint(Type_Coin, sf::Vector2f(26300.f, 1560.f), sf::Vector2f(1.f, 1.f)));


	return true;
}

std::vector<SpawnPoint> Level::GetSpawner()
{
	return m_SpawnLocations;
}