#include "Particle.h"

Particle::Particle(System xSystem, sf::Vector2f xPos, sf::Vector2f xVelocity, sf::Vector2f xSize, sf::Color xColor, float xAngle, float xAngularVel, int xTTL)
{
	mPosition = xPos;
	mVelocity.y = xVelocity.y;
	mSize = xSize;
	mColor = xColor;
	mAngle = xAngle;
	mAngularVel = xAngularVel;
	TTL = xTTL;
	mSprite = xSystem.m_pxSpriteManager->CreateSprite("../assets/fx/circle.png", 0, 0, 32, 32);
	
	if (mColor == sf::Color::Blue)
	{
		mSprite = xSystem.m_pxSpriteManager->CreateSprite("../assets/level1/background/Tileset.png", 0, 0, 0, 0);
		TTL = 10000;
		mSprite.setTextureRect(sf::IntRect(0,1580, 60, 60));
		mVelocity.y -= 1.f;
		if (mVelocity.y < 0.5f)
		{
			mVelocity.y = 0.5f;
		}
	
	}
	mVelocity.x = 2.5f;
}
Particle::~Particle()
{

}
void Particle::Update()
{
	TTL--;
	mPosition.y += -mVelocity.y;
	mPosition.x += -mAngle / 360 * mVelocity.x;
	mSprite.setPosition(mPosition);

	if (mColor == sf::Color::Red)
	{
		Circle.setRadius(20);
		Circle.setFillColor(sf::Color(255, 100, 100, TTL));
		Circle.setPosition(mPosition);
		Circle.setOutlineColor(sf::Color(255, 100, 100, TTL));
		Circle.setScale(mSize);
	}
	mSprite.setScale(mSize);
	mAngle += mAngularVel;

	if (mPosition.x < 0)
	{
		TTL = 0;
	}
	else if (mPosition.y < 720)
	{
		TTL = 0;
	}
}
void Particle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (mColor == sf::Color::Red)
	{
		states.blendMode = sf::BlendMultiply;
		states.texture = mSprite.getTexture();
		target.draw(Circle, states);
	}
	else
	{
		states.blendMode.Add;	
		states.texture = mSprite.getTexture();
		target.draw(mSprite, states);
	}
	//states.transform *= mSprite.getTransform();

}