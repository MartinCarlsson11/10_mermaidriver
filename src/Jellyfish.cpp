#include "stdafx.h"
#include "Jellyfish.h"

Jellyfish::Jellyfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem) :
	IEntity(xType, xPosition, xVelocity, xScrollSpeed, xSystem)

{
	CreateSprite();
	mVelocity = sf::Vector2f(-mScrollSpeed, 0.f);
}

Jellyfish::~Jellyfish()
{
	mAnimationFrames.~AnimateSprite();
}

bool Jellyfish::Update()
{
	mSprite.setPosition(mPosition);

	sf::RectangleShape xCollider1;
	xCollider1.setSize(sf::Vector2f(55.f, 50.f));
	xCollider1.setPosition(sf::Vector2f(mPosition.x + 60.f, mPosition.y + 40.f));
	SetColliderRect1(xCollider1);

	xCollider1.setPosition(sf::Vector2f(mPosition.x + 70.f, mPosition.y + 85.f));
	xCollider1.setSize(sf::Vector2f(35.f, 150.f));
	SetColliderRect2(xCollider1);


	mSprite.setTextureRect(mAnimationFrames.UpdateASprite(0.16f));

	if (mPosition.x + mSprite.getLocalBounds().width < 0)
	{
		return false;
	}

	Move();
	return true;
}

void Jellyfish::Move()
{


	if (mPosition.y < 300)
	{
		mPosition.y = 300;
	}
	if (mPosition.y > 1680)
	{
		mPosition.y = 1680;
	}

	mPosition += mVelocity;
}


sf::Vector2f Jellyfish::GetPosition()
{
	return mPosition;
}
sf::Sprite Jellyfish::GetSprite()
{
	return mSprite;
}

EntityType Jellyfish::GetType()
{
	return mType;
}

void Jellyfish::CreateSprite()
{
	mAnimationFrames.AddFramesFromFile(mSystem.m_pxSpriteManager->GetAnimation("../assets/level1/jellyfish/animation/idle.txt"));
	mSprite = mSystem.m_pxSpriteManager->CreateSprite("../assets/Level1/jellyfish/Jellyfish.png", 0, 0, 0, 0);
}



void Jellyfish::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = mSprite.getTexture();

	target.draw(mSprite, states);
}
