2016-02-15 @ 19:30

Idag har jag fr�mst jobbat med att fixa buggar innan och under dagens playtesting. 
Mestadels handlade det om pointer och array buggar.
Annars har dagen varit upptagen med playtesting och plannering inf�r beta.
Playtesten gick f�rv�nandsv�rt bra och det verkar som att projektet �r p� r�tt v�g.
Fr�n playtesten hittades ett antal buggar, som nu �r rapporterade p� bitbucket issue tracker. 

2016-02-16 @ 19:30

Idag har jag jobbat p� att includera TinyXml f�r att kunna l�sa .tmx filer(Tiled), 
f�r att g�ra det enklare f�r grafikerna att g�ra levels och testa tile systemet.
Funderar �ven p� att implementera liknande funktionalitet f�r Animationer.

Anv�nder TinyXml f�r att det �r "Tiny" och tror den har funktionerna jag s�ker.
Fill�sning �r helt fr�mmande f�r mig, s� gjort mycket reasearch den senaste dagen. 
Men med Tiled f�r man en .tmx fil som man kan l�sa som xml fil. 
Sen �r det "bara" att g� igenom hela xml filen och hantera alla variabler man beh�ver I guess.

Fr�n tiled f�r jag b�de information om tilesets och layermaps. 
S� n�r allt v�l �r implementerat b�r jag inte beh�va r�ra texturerna eller positioner/tilegrid, hoppas kan man ju!

Att l�sa en xml fil verkar r�tt straightforward och tinyxml verkar l�tt att l�ra sig.
�n s� l�nge kan jag l�sa in tile sets. S� beh�ver bara l�sa in tilemaps och sen lagra variablerna p� n�got smart s�tt.
Sedan anv�nda variablerna f�r att kunna rendera tilesetsen p� r�tt tile och sedan hantera logiken(scroll, update, collision).

F�r tillf�llet �r Mapgrid i spelet dynamiskt allocerad och har lika m�nga lager som .tmx filen. 

Tanken �r att Mapgrid ska lagras i en 3d vector. 
S� varje lager �r en ny 2d vector med information om tiles i x, y, w, h, tileN.
F�r att komma �t en tile i mapgrid: Mapgrid[Lager][y][x]->w,h, tileN.

Sen har det kommit upp n�gra fr�gor ang�ende hur leveln ska genereras. 
Tills vidare �r tanken att man bygger ett antal delar av leveln och sedan blandar mellan dom olika delarna f�r att ge
spelaren k�nslan av att banan f�r�ndras

2016-02-17 @ 19:30

Idag har jag fortsatt jobba med att l�sa xml filer och anv�nda datan. Men har blivit sidetrackad p� att g�ra lite f�rb�ttringar p� alpha versionen till presentationen
Dom flesta av f�r�ndringarna �r att fixa/implementera det som playtesten gav oss feedback p�. 
T.ex. Harpunens collision. Som snart anv�nder en Circle vs Rectangle function. 
Och sedan visuella och balanserings saker.

2016-02-18 @ 19:30

Idag har jag fokuserat p� gameplay f�rb�ttringar till alpha versionen, baserat p� feedback fr�n att Marcus speltestade spelet.
Inte hunnit med s� mycket p� grund av att vi beh�vde lyssna igenom alpha presentationen och se till att det l�t bra.

2016-02-19 @ 21:30
Idag var det alpha presentation. M�nga id�er har v�ckts, fr�mst p� grund av att man ser vad folk har missat i sina projekt.
N�gra exempel: Knockback n�r man skjuter harpunen, particle engine, dekadens, och annat roligt.

Projektet ser ut att vara i samma stadie som m�nga andras. Men kod m�ssigt och struktur m�ssigt finns det m�nga h�l som beh�ver t�ppas igen.
Det mesta beror p� att jag �r slarvig och att jag skriver kod f�r att den ska vara l�tt att ta bort. 
S� jag har mycket kod i vissa funktioner och klasser som b�r flyttas till egna klass/funktioner. 


2016-02-22 @ 18:00
Nu �r tile systemet klart f�r att anv�ndas av grafikerna. 

Hade lite problem med att rita ut alla lager. Men till slut har jag lyckats. Det var mycket enklare �n vad jag trodde. 
Det jag hade problem med var att jag f�rs�kte rita ut alla enskilda tiles som egna sprites. 
Tillsammans med att jag f�rs�kte �ndra positionen p� alla individuella tiles n�r jag scrollade bakgrunden, blev spelet ospelbart. 
 
Nu ritar jag ut varje lager av bakgrunden i varsin sf::VertexArray. 
Sedan anv�nder jag en sf::view f�r varje lager och d� manipulerar jag den sf::viewen f�r att scrolla bakgrunden.

S� nu kan grafikerna level designa s� mycket dom vill, v�ntar p� feedback f�r att se om allt fungerar som det ska.
Jag kommer antagligen bli tvungen att optimisera utritningen av VertexArrayen. F�r tillf�llet ritar jag ut en hel bild f�r varje lager.
Men det hade ju r�ckt med att rita ut den delen av bilden som beh�ver visas.

2016-02-23 @ 18:00

Idag har jag inte gjort n�got!

2016-02-24 @ 21:55

Idag har jag fr�mst jobbat med att st�da koden och implementera fill�sning f�r animationsrutor.
Men �ven jobbat lite p� soundmapping, kollision och animationmapping. 

Kod st�dning handlade mest om att snygga till fiende listorna och f�r att bli av med redundant kod som "if object != nullptr" innan update iterering.
Samtidigt att g�ra iterering av listorna lite mer stabil, och/eller snyggare. L�rde mig om range-based for loops. 
S� nu anv�nder jag det i loopar d�r jag bara beh�ver �t objekten och uppdatera eller rita ut dom. Lite snyggare och lite mer l�sbart.
(�ven tagit bort massor av icke-anv�nda funktioner, variabler och flyttat tempor�ra funktioner till sina platser eller tagit bort dom)

Sound mappingen �r ganska of�rdig. V�ntar fortfarande p� att f� tillg�ng till alla ljud s� att jag vet exact vad f�r ljud som kommer anv�ndas.
Har dock b�rjat med en del ljud som jag f�rv�ntar mig att vi anv�nder. Tanken �r att whiteboxa ljuden till betan och sedan byta ut dom/finslipa dom. 

Nu l�ser jag �ven in information om animationer och bilder fr�n filer. Inte dom snyggaste eller b�sta l�sningarna, 
men jag �r ganska oerfaren n�r det g�ller fill�sning/skrivning s� jag l�r mig mycket! Funktionaliteten �r dock d�r,
s� grafikerna kan sl�nga in animationer och updatera existerande bilder utan att jag ska beh�va r�ra det.

Kollision �r n�got jag kommer jobba p� imorgon och fram�t helgen. Har n�gra olika id�er som jag bollar med. 
Varit lat och inte implementerat circle vs rectangle collision checking �n, 
men har varit lite os�ker p� om jag ska g�ra det eller inte. Det visar sig!

�ven fixat n�gra sm� buggar och annat krafs ( vsync on, harpunens tillbaka funktion etc..)

�ven kikat lite p� ljusmotor och partikelmotor som �r tv� saker jag g�rna hade viljat implementera. 
Det verkar inte f�r avancerat f�r att hinnas med! wish me luck!

2016-02-25 @ 21:55
Idag har jag omarbetat High score systemet och Luft systemet. Ser lite snyggare ut kodm�ssigt och �r l�ttare att h�lla reda p�. 


2016-02-28 @ 21:55
�ndrat hur animation frames laddas in. Nu anv�nder jag en std::map f�r att h�lla koll p� om filen som inneh�ller 
framedata redan har l�sts, och i s�fall anv�nds den datan ist�llet f�r att l�sa in filen igen. 
Det var inte s� smart att l�sa in en fil varje g�ng en npc spawnades.

Ny hud, inte gjort en ordentlig klass f�r den �n, men snart.


2016-02-29 @ 22:55
Idag har jag inte gjort n�got! Upptagen med speltest osv.

2016-03-01 @ 23:30
Nytt Kollisions system. Nu anv�nder jag SAT f�r harpunens kollision. 
I och med att harpunens skaft �ven beh�ver kollision mot diverse enemies k�nns det som att SAT Rotated Box kollision var det mest logiska att anv�nda.

Arbetar p� feedbacken vi fick fr�n speltestet. Fr�mst kaptenens r�relser och att ge spelaren lite mer frihet i input.
Pr�var en massa olika mekaniker f�r att f� spelaren att inte k�nna sig frihetsber�vad.

Implementerat en tempor�r parallax hanterare. 
2016-03-02 @ 23:00
Testat en del olika typer av movement. Kom fram till att beh�lla det tidigare r�relse systemet men att l�ta spelaren r�ra sig i h�gre frekvens. 
Nu kan spelaren r�ra sig var 0.7e sekund.
Implementerat Rom och Blowfish. Updaterat Kaptenens Sprite och Animations, v�ntar fortfarande p� att assets ska bli klara.

2016-03-03 @ 24:00
Idag har jag mest h�llt p� med research dels design av AI handler och dels lite research kring PreLoading. �ven b�rjat prototypa en partikel motor.

https://docs.google.com/presentation/d/1JaWpvxcJE64AOVr2qvitqUAJjpNLJA8a2J5eV5m_uf4/edit?usp=sharing