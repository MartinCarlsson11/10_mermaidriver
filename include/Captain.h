#pragma once
#include "IEntity.h"
class Harpoon;

class Captain : public IEntity
{
public:
	Captain(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Captain();
	bool Update();
	bool Update(sf::View xView);
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	EntityType GetType();

	void TakeDamage(int p_damage, bool xElectric);
	int GetHP();
	void RestoreHealth(int p_HealthRestore);
	bool BubblePowerup();
	void SetPowerUpActive(bool xState);
	
	static sf::Vector2f GetCaptainPosition();

private:
	void CreateSprite();
	void UpdateCollider();
	sf::Clock limitMovement;
	sf::Time xAccumulator;


	virtual void draw(sf::RenderTarget & 	target,
		sf::RenderStates 	states
		)		const;
	bool setFrames;
	void Move();
	void Deacceleration();
	sf::Clock limitDamage;
	sf::Time xDamageAccu;
	bool m_xTakeDamage;
	int m_xHitPoints;

	float m_xAngle;
	bool m_xBubblePowerup;
	void SetNewAnimation(AnimateSprite xAnimateSprite);
	//Temporary Animation testing
	
	bool setThrowOnce;

	bool BubbleFixIdleTemp;
	bool ResetFromHurtFrame;

	sf::Sprite mSprite_Electric;
	sf::Sprite mSprite_Idle;
	sf::Sprite mSprite_SwimForward;
	sf::Sprite mSprite_Throw;
	sf::Sprite mSprite_Hurt;

	sf::Sprite mArm;

	sf::Sprite mSprite_Arm_Idle_Empty;
	sf::Sprite mSprite_Arm_Idle_Harpoon;
	sf::Sprite mSprite_Arm_Idle;



	sf::Sprite mSprite_Arm_SwimForward;
	sf::Sprite mSprite_Arm_SwimForward_Harpoon;
	sf::Sprite mSprite_Arm_SwimForward_Empty;
	sf::Sprite mSprite_Arm_SwimForward_Reel;
	sf::Sprite mSprite_Arm_Reel;


	AnimateSprite mAnimationFrames_Hurt;
	AnimateSprite mCurrentFrames;
	AnimateSprite mCurrentArmFrames;

	AnimateSprite mAnimationFrames_Arm_Idle;

	AnimateSprite mAnimationFrames_Arm_Empty;
	

	AnimateSprite mAnimationFrames_Arm_SwimForward;

	AnimateSprite mAnimationFrames_Arm_SwimForward_Empty;
	AnimateSprite mAnimationFrames_Arm_SwimForward_Reel;
	AnimateSprite mAnimationFrames_Arm_Reel;
	AnimateSprite mAnimationFrames_Arm_Throw;

	AnimateSprite mAnimationFrames_Electric;
	AnimateSprite mAnimationFrames_Idle;
	AnimateSprite mAnimationFrames_SwimForward;
	AnimateSprite mAnimationFrames_Throw;
	AnimateSprite mAnimationFrames_Windup;
	static sf::Vector2f mCurrentPosition;

	bool mAnimationIsNotIdle;
};