#pragma once
class Mouse
{
public:
	Mouse();
	~Mouse();
	static bool DidButtonGetPressed(int p_iIndex);
	static bool DidButtonGetReleased(int p_iIndex);
	static void SetButton(int p_iIndex, bool p_bValue);
	static void SetPressButton(int p_iIndex, bool p_bValue);
private:
	static bool m_abButtons[3];
	static bool m_abPressButtons[3];
};
