#pragma once
#include "IState.h"
#include <thread>
#include <mutex>
#include <future>
#include "Level.h"
#include "ParallaxHandler.h"

class LevelLoad : public IState
{
public:
	LevelLoad(System& p_xSystem);
	~LevelLoad();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();
	
private:
	bool LoadTheLevel();
	Level* m_xLevel;
	ParallaxHandler* m_xParallaxHandler;
	System m_xSystem;
	sf::Text text;
	sf::Font font;
};