#pragma once
#include "IEntity.h"

class Bird : public IEntity
{
public:
	Bird(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Bird();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();

	bool Update();
	EntityType GetType();
private:
	void CreateSprite();
	void Move();
	bool mReachEnd;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};