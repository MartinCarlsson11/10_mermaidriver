#pragma once
#include "stdafx.h"
#include "IState.h"

enum ButtonType {
	Button_Play,
	Button_Quit,
	Button_Continue,
	Button_HighScores,
	Button_MainMenu
};



class Menu : public sf::Drawable, public sf::Transformable
{
#define MAX_NUMBER_OF_ITEMS 4
public:
	Menu();
	Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3, ButtonType xType4, sf::FloatRect xPos4, ButtonType xType5, sf::FloatRect xPos5);
	Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3, ButtonType xType4, sf::FloatRect xPos4);
	Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2, ButtonType xType3, sf::FloatRect xPos3);
	Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1, ButtonType xType2, sf::FloatRect xPos2);
	Menu(System xSystem, ButtonType xType1, sf::FloatRect xPos1);
	~Menu();

	bool Update();
	int Menu::ReturnSelectedItem();
private:

	System mSystem;
	void CreateSprite();
	void SetTypes();
	std::vector<ButtonType> mType;
	std::vector<sf::FloatRect> mPos;
	std::vector<sf::Sprite> mSprite;
	std::vector<sf::Sprite> mHighLightSprite;

	std::vector<sf::Sprite> mActiveSprite;

	int mSelectedItem;
	void MoveUp();
	void MoveDown();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};