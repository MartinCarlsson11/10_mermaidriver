#pragma once
#include "IState.h"
#include "stdafx.h"
class Preload : public IState
{
public:
	Preload(System& p_xSystem);
	~Preload();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();
private:
	sf::Sprite m_xSprite;
	System m_xSystem;
	sf::Text text;
	sf::Font font;
	std::vector<const char*> TexturePaths;
	std::vector<const char*> AnimationPaths;
	float Progress; 
	int LoadTextures;
	int LoadAnimations;
	float Finished;
};