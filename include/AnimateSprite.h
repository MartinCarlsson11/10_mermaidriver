#pragma once
#include "stdafx.h"
struct FrameData {
	sf::IntRect m_xRegion;
	float m_fDuration;
};

class AnimateSprite
{
public:
	AnimateSprite();
	~AnimateSprite();

	void AddFrame(int p_iX, int p_iY, int p_iWidth, int p_iHeight, float p_fDuration);
	void AddFramesFromFile(std::vector<FrameData> xFrames);
	sf::IntRect UpdateASprite(float p_fDeltaTime);
	sf::IntRect UpdateASprite(float p_fDeltaTime, bool xRepeat);
	bool AnimationFinish();
	void AnimationStop();
	void Reset();
	void AnimationResume();
private:
	bool AnimationFinished;
	bool AnimationStopped;
	FrameData Myframes;
	std::vector<FrameData> m_axFrames;
	int m_iIndex;
	float m_fCurrentDuration;
};

