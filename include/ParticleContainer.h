#include "Particles.h"

class ParticleContainer
{
public:
	ParticleContainer(const int maxNumParticles);
	~ParticleContainer();

	void Update(float deltaTime);
	void Render();

private:

	Particle* mParticles;
	int mNumParticles;
	const int mMaxParticles;



};