#pragma once
#include "stdafx.h"
class DrawManager 
{
public:
	DrawManager(sf::RenderWindow* p_xWindow);
	~DrawManager();
	void Draw(sf::Sprite p_xSprite, sf::Vector2f p_xDestination);
	void Draw(sf::Text p_xText, sf::Vector2f p_xDestination);
	void Draw(sf::Drawable &x_rect);
	void Clear();
	void Display();
	void SetView(sf::View p_xView);
	void SetDefaultView();
private:
	sf::RenderWindow *m_pxWindow;
};