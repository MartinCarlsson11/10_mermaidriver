#pragma once
#include "IEntity.h"
class Anglerfish : public IEntity
{
public:
	Anglerfish(sf::Vector2f p_position, System xSystem);
	~Anglerfish();
	bool Update();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	sf::Vector2f GetVelocity();
	void CreateSprite(System x_System);
private:
	void Move();

	sf::Vector2f m_xPosition;
	sf::Vector2f m_xVelocity;
	sf::Sprite m_xSprite;

	AnimateSprite m_xAnimationFrames;
	sf::Clock limitMovement;
	sf::Time xAccumulator;
};