#pragma once
#include <list>
#include "stdafx.h"
#include "Particle.h"
#include <Random>

class ParticleHandler : public sf::Drawable
{
public:
	ParticleHandler(System xSystem, int xMaxNum, sf::Vector2f xlocation, sf::Color xColor);
	~ParticleHandler();

	void Update(float deltatime, sf::Vector2f xEmitPos, float xMagnitude);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	std::default_random_engine generator;

	sf::Color RandomColor;
	System mSystem;
	Particle* GenerateNewParticle();
	void RemoveParticle();
	sf::Vector2f mEmitterLoc;
	float mFrequency;
	std::vector <Particle*> mParticles;
	int mMaxParticleNum;
	int mActiveParticles;
};