#pragma once
class HighscoreManager {
public:
	HighscoreManager();
	~HighscoreManager();
	int GetHighscore();
	int GetBonus();
	int GetNewPoints();
	void AddHighscore(int points , int bonus);
	void AddHighscore(int points);
	void ResetBonus();

private:
	void UpdateHighscore();
	int Highscore;
	int Bonus;
	int NewPoints;
};