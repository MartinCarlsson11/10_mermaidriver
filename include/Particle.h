#pragma once
#include "stdafx.h"
#include "IState.h"
class Particle : sf::Drawable
{
public:
	Particle(System xSystem, sf::Vector2f xPos, sf::Vector2f xVelocity, sf::Vector2f xSize, sf::Color xColor, float xAngle, float xAngularVel, int xTTL);
	~Particle();
	void Update();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	int TTL;
private:
	sf::Sprite mSprite;
	sf::Vector2f mVelocity;
	sf::Vector2f mPosition;
	sf::Vector2f mSize;
	sf::Sprite mParticleSprite;
	float mAngle;
	float mAngularVel;
	sf::Color mColor;
	sf::CircleShape Circle;
	sf::Vector2f mScale;

	friend class ParticleContainer;

	Particle(const Particle& rhs);
};