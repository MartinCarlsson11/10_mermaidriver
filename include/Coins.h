#pragma once
#include "IEntity.h"

class Coins : public IEntity
{
public:
	Coins(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Coins();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	bool Update();
	EntityType GetType();
private:
	void CreateSprite();
	void Move();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};