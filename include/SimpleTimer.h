#pragma once
#include "stdafx.h"
class SimpleTimer
{
public:
	SimpleTimer(int p_interval, sf::Time CurrentTime);
	~SimpleTimer();
	int Ready();
	int Check();
	void Update(sf::Time CurrentTime);
	int GetTime();
private:
	int m_xLastTime;
	int m_xCurrentTime;
	int m_xAccumulator;
	int m_xTotal;
	int m_xInterval;
};