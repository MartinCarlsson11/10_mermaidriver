#pragma once
#include "Captain.h"
class AirSystem {
public:
	AirSystem();
	~AirSystem();
	float GetAirLevel();
	void Update(Captain xCaptain);
	void SetPowerupActive(bool xTrue);
private:
	float AirLevel;
	bool AirLoss;
	float MaxAirLevel;
};