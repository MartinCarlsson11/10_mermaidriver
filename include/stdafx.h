#pragma once
//sf::stuff
#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


#if !defined(NDEBUG)
#pragma comment (lib, "sfml-main-d.lib")
#pragma comment (lib, "sfml-window-d.lib")
#pragma comment (lib, "sfml-system-d.lib")
#pragma comment (lib, "sfml-graphics-d.lib")
#pragma comment (lib, "sfml-audio-d.lib")

#else
#pragma comment (lib, "sfml-main.lib")
#pragma comment (lib, "sfml-window.lib")
#pragma comment (lib, "sfml-system.lib")
#pragma comment (lib, "sfml-graphics.lib")
#pragma comment (lib, "sfml-audio.lib")

#endif