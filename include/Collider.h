#pragma once
#include "stdafx.h"
class Collider
{
public:
	~Collider();
	Collider(sf::CircleShape xColliderCircle);

	Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
		sf::RectangleShape xColliderRect3, sf::RectangleShape xColliderRect4);

	Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
		sf::RectangleShape xColliderRect3);

	Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2);

	Collider(sf::CircleShape xColliderCircle, sf::RectangleShape xColliderRect1);

	Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
		sf::RectangleShape xColliderRect3, sf::RectangleShape xColliderRect4);

	Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2,
		sf::RectangleShape xColliderRect3);

	Collider(sf::RectangleShape xColliderRect1, sf::RectangleShape xColliderRect2);

	Collider(sf::RectangleShape xColliderRect1);

	sf::RectangleShape GetColliderRect1();
	sf::RectangleShape GetColliderRect2();
	sf::RectangleShape GetColliderRect3();
	sf::RectangleShape GetColliderRect4();
	sf::CircleShape GetColliderCircle1();

	void Collider::SetColliderRect1(sf::RectangleShape xRect);
	void Collider::SetColliderRect2(sf::RectangleShape xRect);
	void Collider::SetColliderRect3(sf::RectangleShape xRect);
	void Collider::SetColliderRect4(sf::RectangleShape xRect);
	void Collider::SetColliderCircle1(sf::CircleShape xCircle);

private:
	sf::RectangleShape m_xColliderRect1;
	sf::RectangleShape m_xColliderRect2;
	sf::RectangleShape m_xColliderRect3;
	sf::RectangleShape m_xColliderRect4;
	sf::CircleShape m_xColliderCircle1;

protected:
	Collider();
};

