#pragma once
#include "stdafx.h"

class SoundManager {
public:
	SoundManager();
	~SoundManager();
	void PlaySound(sf::Sound& xSound, bool xPitch);
	void PlayMusic(sf::Music& xMusic);
	void StopMusic();

	sf::SoundBuffer SwimBuffer;
	sf::Sound SwimSound;

	sf::SoundBuffer Birdattack;
	sf::SoundBuffer	BirdDeath;
	sf::SoundBuffer	BirdSwim;
	sf::SoundBuffer BirdWater;

	sf::SoundBuffer Blowfishdeath;
	sf::SoundBuffer Blowfishswim;

	sf::SoundBuffer CoinPickup;

	sf::SoundBuffer Flyfishattack;
	sf::SoundBuffer Flyfishswim;

	sf::SoundBuffer JellyfishAttack;
	sf::SoundBuffer JellyfishSwim;
	sf::SoundBuffer JellyfishDeath;

	sf::SoundBuffer SwordfishDeath;
	sf::SoundBuffer SwordfishLunge;

	sf::SoundBuffer PowerupBubbleBuffer;
	sf::SoundBuffer PowerupSwordBuffer;
	sf::SoundBuffer PowerupChainBuffer;

	sf::SoundBuffer CaptainRefill;
	sf::SoundBuffer CaptainReel;
	sf::SoundBuffer CaptainTakeDamage;
	sf::SoundBuffer CaptainBirdDamage;
	sf::SoundBuffer CaptainElectricDamage;
	sf::SoundBuffer CaptainThrow;
	sf::SoundBuffer CaptainDeath;
	sf::SoundBuffer CaptainDrink;

	sf::SoundBuffer HarpoonChargeComplete;

	
	sf::Sound BirdattackSound;
	sf::Sound BirdDeathSound;
	sf::Sound BirdSwimSound;
	sf::Sound BirdWaterSound;

	sf::Sound BlowfishdeathSound;
	sf::Sound BlowfishswimSound;
	
	sf::Sound CoinPickupSound;
	
	sf::Sound FlyfishswimSound;

	sf::Sound JellyfishAttackSound;
	sf::Sound JellyfishSwimSound;
	sf::Sound JellyfishDeathSound;
	
	sf::Sound SwordfishDeathSound;
	sf::Sound SwordfishLungeSound;
	
	sf::Sound PowerupBubbleBufferSound;
	sf::Sound PowerupSwordBufferSound;
	sf::Sound PowerupChainBufferSound;
	
	sf::Sound CaptainRefillSound;
	sf::Sound CaptainReelSound;
	sf::Sound CaptainTakeDamageSound;
	sf::Sound CaptainThrowSound;
	sf::Sound CaptainDeathSound;

	sf::Sound CaptainDrinkSound;
	sf::Sound CaptainBirdDamageSound;
	sf::Sound CaptainElectricDamageSound;

	sf::Sound HarpoonFullChargeSound;

	sf::Music UnderwaterEffect;
	sf::Music Music;
	sf::Music WinMusic;
	sf::Music DeathMusic;
	sf::Music CurrentlyPlaying;
private:

};