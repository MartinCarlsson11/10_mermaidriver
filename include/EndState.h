#pragma once
#include "HighscoreManager.h"
#include <fstream>
#include "IState.h"
#include "stdafx.h"
#include "Menu.h"
class EndState : public IState
{
public:
	EndState(System& p_xSystem, bool xWin, HighscoreManager xHighscore);
	~EndState();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();
private:
	sf::Sprite m_xSprite;
	System m_xSystem;
	bool WinOrLose;

	std::fstream doc;
	std::string line;

	std::string Name;
	std::string Highscore;

	sf::Font font;

	std::vector<std::string> Entries;

	Menu mMenu;
	sf::Text Entry1;
	sf::Text Entry2;
	sf::Text Entry3;
	sf::Text Entry4;
	sf::Text Entry5;
};