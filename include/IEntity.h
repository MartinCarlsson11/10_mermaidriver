#pragma once
#include "stdafx.h"
#include "AnimateSprite.h"
#include "IState.h"
#include "Collider.h"
#include "ParticleHandler.h"

enum EntityType {
	Type_Captain,
	Type_Bird,
	Type_Coin,
	Type_Flyfish,
	Type_Harpoon,
	Type_Jellyfish,
	Type_Powerup,
	Type_Blowfish,
	Type_Swordfish,
	Type_Rum,
	Type_Projectiles
};

class Collider;

class IEntity : public Collider, public sf::Drawable, public sf::Transformable
{
public:
	IEntity(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem){
		mType = xType;
		mPosition = xPosition;
		mVelocity = xVelocity;
		mScrollSpeed = xScrollSpeed;
		mSystem = xSystem;
		mVelocity = sf::Vector2f(0, 0);
	}
	virtual sf::Sprite GetSprite() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual void CreateSprite() = 0;

	virtual bool Update() = 0;
	virtual EntityType GetType() = 0;
private:
	IEntity() {  };

protected:
	EntityType mType;

	virtual void Move() = 0;
	sf::Vector2f mPosition;
	sf::Vector2f mVelocity;

	float mScrollSpeed;
	sf::Vector2i mWorldDimenions;

	System mSystem;

	sf::Sprite mSprite;
	AnimateSprite mAnimationFrames;
};