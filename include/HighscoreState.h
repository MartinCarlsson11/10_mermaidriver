#pragma once
#include "Menu.h"
#include "IState.h"
#include "stdafx.h"

class HighscoreState : public IState
{
public:
	HighscoreState(System& p_xSystem);
	~HighscoreState();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();

private:
	sf::Font font;

	std::string Entries[20];


	sf::Text Entry1;
	sf::Text Entry2;
	sf::Text Entry3;
	sf::Text Entry4;
	sf::Text Entry5;
	sf::Text Entry6;
	sf::Text Entry7;
	sf::Text Entry8;
	int mSelectedState;
	System m_xSystem;
	sf::Sprite SplashScreen;
	Menu* mMenu;
};