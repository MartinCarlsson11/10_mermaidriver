#pragma once
#include "stdafx.h"
#include "Captain.h"
class ParallaxHandler
{
public:
	ParallaxHandler();
	~ParallaxHandler();
	///Layer information goes here
	float GetScrollSpeed(int xArray);
	std::vector<sf::View*> GetLayers();
	void ScrollLayers(float YScroll);
	void ScrollY(Captain xCaptain);
private:
	std::vector<sf::FloatRect*> LayerPositions;
	std::vector<float> ScrollSpeed;
	std::vector<sf::View*> Layers;
};
