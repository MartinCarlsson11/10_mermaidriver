#pragma once
#include "IState.h"
#include "IEntity.h"
#include "Level.h"
#include "AirSystem.h"
#include "HighscoreManager.h"
#include <vector>
#include "stdafx.h"
#include <memory>

#include "SimpleTimer.h"
#include "AnimateSprite.h"
#include "ParallaxHandler.h"
#include "ParticleHandler.h"

#include "Captain.h"
#include "Harpoon.h"
#include "Jellyfish.h"
#include "PowerUp.h"
#include "Swordfish.h"
#include "Bird.h"
#include "Flyfish.h"
#include "Blowfish.h"
#include "Rum.h"
#include "Coins.h"
#include "DeadEntity.h"
#include "Projectiles.h"


class Captain;
class Harpoon;
class Jellyfish;
class PowerUp;
class AnimateSprite;
class SimpleTimer;
class Swordfish;
class Flyfish;
class Bird;
class Blowfish;
class Rum;
class Coins;
class HighscoreManager;


class ParallaxHandler;
class ParticleHandler;
class DeadEntity;
class Projectiles;
struct SpawnPoint;
enum EntityType;

class GameState : public IState
{
public:
	GameState(System& p_xSystem);
	~GameState();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();
private:
	System m_xSystem;
	bool DebugHitboxes;

	sf::FloatRect GameViewPosition;
	sf::View GameView;

	Level* m_xLevel;
	AirSystem m_xAirSystem;
	HighscoreManager m_xHighScore;
	ParallaxHandler* m_xParallaxHandler;
	std::vector<sf::View*> DrawLayers;


	sf::Sprite m_xSurfaceSprite;
	AnimateSprite m_xSurfaceAnimation;

	ParticleHandler* mParticleHandler;
	ParticleHandler* mLevelParticles;

	Captain* m_pxCaptain;
	Harpoon* m_pxHarpoon;


	SimpleTimer* m_xPowerupTimer;
	sf::Clock m_xPowerupClock;
	sf::Time m_xPowerupTime;

	std::vector<IEntity*> mEntityVector;

	std::vector<DeadEntity*> m_pxDeadEntityVector;
	std::vector<Projectiles*> mProjectiles;
	std::vector<SpawnPoint> mSpawnPoints;

	//temporary hud
	sf::Font* HighscoreFont;
	sf::Text HighscoreText;
	sf::Sprite m_xFeedbackDamage;
	sf::Sprite m_xFeedbackDrown;
	sf::Sprite m_xHudElementHead;
	sf::Sprite m_xHudElementHPbar;
	sf::Sprite m_xHudElementAirbar;
	sf::Sprite m_xHudElementAirStatus;
	sf::Sprite m_xHudElementHighscore;
	sf::Sprite m_xHudElementPowerups;
	sf::Sprite m_xHEPowerupBubble;
	sf::Sprite m_xHEPowerupSword;
	sf::Sprite m_xHEPowerupChain;
	sf::Sprite mBubbleFeedback;
	bool mTutorialMouse1Fade;
	bool mTutorialMouse0Fade;
	bool mTutorialWFade;
	bool mTutorialAFade;
	bool mTutorialSFade;
	bool mTutorialDFade;

	float mTutorialMouse1Fader;
	float mTutorialMouse0Fader;
	float mTutorialWFader;
	float mTutorialAFader;
	float mTutorialSFader;
	float mTutorialDFader;


	sf::Sprite mTutorialMouse1;
	sf::Sprite mTutorialMouse0;
	sf::Sprite mTutorialW;
	sf::Sprite mTutorialA;
	sf::Sprite mTutorialS;
	sf::Sprite mTutorialD;

	sf::Sprite m_xCursorSprite;

	sf::Sprite mWinChest;
	AnimateSprite mAnimateCursor;

	int m_iFlashDrown;
	bool mbDrownFlashScreen;
	int m_iFlashDamage;
	bool mbDamageScreen;

	bool WinGame;
	bool ElectricityDamage;
};
