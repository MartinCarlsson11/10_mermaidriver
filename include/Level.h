#pragma once
#include "IState.h"
#include "IEntity.h"
#include "tinyxml.h"
#include "tinystr.h"
struct SpawnPoint {
public:
	SpawnPoint(EntityType Type, sf::Vector2f Pos, sf::Vector2f Size)
	{
		mType = Type;
		mPos = Pos;
		mSize = Size;
	}
	EntityType mType;
	sf::Vector2f mPos;
	sf::Vector2f mSize;
	 
};

class Level : public sf::Drawable, public sf::Transformable
{
public:
	Level(SpriteManager* xSpriteManager);
	~Level();
	std::vector<SpawnPoint> GetSpawner();
	void MoveSpawner(int ScrollSpeed);

	void NextLayer();
	int GetLayers();
private:

	TiXmlElement* elem;
	TiXmlElement *root;
	TiXmlElement* textureElem;
	TiXmlElement* dataElem;
	TiXmlElement* gidElem;

	sf::Texture* mTexture;

	std::vector<sf::VertexArray> m_vertices;
	int VertexArrayCount;
	int CurrentLayer;
	int LayerCount;
	sf::Sprite m_Sprite;
	bool LoadTiles(SpriteManager* xSpriteManager);

	std::vector<SpawnPoint> m_SpawnLocations;
	bool LoadSpawner();
	


	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	
	
};