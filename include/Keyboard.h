#pragma once
class Keyboard
{
public:
	Keyboard();
	~Keyboard();
	static bool IsKeyDown(int p_iIndex);
	static void SetKey(int p_iIndex, bool p_bValue);
private:
	static bool m_abKeys[256];
};