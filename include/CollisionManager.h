#pragma once
#include "stdafx.h"
///This code is translated from Simple Collision Detection for SFML 2 by ahnonay
class OrientedBoundingBox
{
public:
	sf::Vector2f Points[4];
	OrientedBoundingBox(const sf::RectangleShape Object);
	void ProjectOntoAxis(const sf::Vector2f& Axis, float& Min, float& Max);
};

class CollisionManager {
public:

	static bool CheckCollisionAABB(sf::Sprite xCollider1, sf::RectangleShape xCollider2);
	static bool CheckCollisionAABB(sf::RectangleShape xCollider1, sf::RectangleShape xCollider2);
	static bool CheckCollisionCircleBox(sf::RectangleShape xCollider1, sf::CircleShape xCollider2);
	///This code is translated from Simple Collision Detection for SFML 2 by ahnonay
	static bool CheckCollisionRRectangle(sf::RectangleShape xCollider1, sf::RectangleShape xCollider2);
	static bool CheckCollisionPoint();
};