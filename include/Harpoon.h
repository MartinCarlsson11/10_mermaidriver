#pragma once
#include "IEntity.h"
#include "SimpleTimer.h"

class Captain;


class Harpoon : public IEntity
{
public:
	Harpoon(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Harpoon();
	bool Update(sf::View xView, Captain xCaptain, bool ElectricityDamage);
	bool Update();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	sf::Vector2f GetVelocity();
	void CreateSprite();
	EntityType GetType();

	bool fullWindup;
	void UpdateRope(Captain xCaptain, bool xToggleElectricity);
	void Shoot(Captain xCaptain, sf::Vector2f xVelocity);
	bool HarpoonState();
	void HarpoonInactive();
	static bool GetHarpoonState();
	
	bool IsVisible();
	float GetWindupTime();
	sf::RectangleShape HarpoonRope();
private:
	sf::CircleShape xColliderCircle;
	sf::RectangleShape xCollider1;
	sf::RectangleShape xCollider2;

	void Move();
	void Deacceleration();
	void ResetHarpoon(sf::Vector2f xTargetPos);

	sf::Clock SimpleTimerClock;
	SimpleTimer* m_pxThrowTimer;


	sf::Vector2f Velocity;

	sf::Sprite m_xRopeSprite;
	sf::RectangleShape m_xRope;
	
	bool xShootRight, xShootLeft, xShootUp, xShootDown;
	float m_xAngle;
	bool PossessHarpoon;
	bool xShoot;
	int TimeKeeper;
	
	float m_xRotateAngle;


	bool m_xHarpoonState;
	static bool mHarpoonStateStatic;
	bool m_bIsVisible;
	bool m_bReturnHarpoon;
	sf::Vector2f m_xDeceleration;
	sf::Vector2f m_xDirection;

	sf::Clock m_pxElectricClock;
	sf::Time ElectricAccu;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

