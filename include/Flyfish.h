#pragma once
#include "IEntity.h"

class Flyfish : public IEntity
{
public:
	Flyfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Flyfish();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	bool Update();
	EntityType GetType();
private:
	void CreateSprite();
	void Move();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Clock mSoundClock;
	sf::Time mSoundTime;
};