#pragma once
#include "stdafx.h"
#include <map>
#include "AnimateSprite.h"

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();
	sf::Sprite CreateSprite(const char* p_sFilepath, float p_iX, float p_iY, int p_iW, int p_iH);
	sf::FloatRect SpriteManager::SetTextureRect(sf::Sprite spriteref);
	std::vector<FrameData> GetAnimation(const char* p_sFilePath);
private:
	std::vector<FrameData> m_axFrames;
	std::map<const char*, std::vector<FrameData>> m_apxFrameData;
	FrameData Myframes;

	std::vector<sf::Sprite> m_axSprites;
	std::map<const char*, sf::Texture*> m_apxTextures;
};

