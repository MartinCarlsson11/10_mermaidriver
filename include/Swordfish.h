#pragma once
#include "IEntity.h"
#include "SimpleTimer.h"
#include <memory>
class Swordfish : public IEntity
{
public:
	Swordfish(EntityType xType, sf::Vector2f xPosition, sf::Vector2f xVelocity, float xScrollSpeed, System xSystem);
	~Swordfish();
	sf::Sprite GetSprite();
	sf::Vector2f GetPosition();
	bool Update();
	EntityType GetType();

	void FindCaptain();

private:
	void CreateSprite();
	void Move();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Vector2f mTargetPosition;

	bool ReadyToCharge();

	bool mFindYPositive;
	bool mFindYNegative;
	sf::Time mChargeAccumulator;
	sf::Clock mChargeClock;

	bool mSetLoopAnimationTrue;

	sf::Sprite mSpriteCharge;
	sf::Sprite mSpriteIdle;

	AnimateSprite mAnimationFrames_Loop;
	AnimateSprite mAnimationFrames_Idle;
	AnimateSprite mAnimationFrames_Charge;
};