#pragma once
#include <string>
#include "stdafx.h"
#include "SpriteManager.h"
#include "SoundManager.h"
#include "StateManager.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "DrawManager.h"

class Engine
{
public:
	Engine();
	~Engine();
	bool Init();
	void Update();
	void Shutdown();
	void HandleEvents();
	std::string GetTextEntered();
	void ResetTextEntered();
private:
	bool m_bRunning;
	sf::Event m_xEvent;
	sf::RenderWindow *m_pxWindow;
	SpriteManager* m_pxSpriteManager;
	DrawManager* m_pxDrawManager;
	StateManager* m_pxStateManager;
	SoundManager* m_pxSoundManager;
	//Update Limiter
	sf::Clock m_xClock;
	sf::Time m_xTargetTime;
	sf::Time m_xAccumulator;

	std::string TextEnteredString;

};

