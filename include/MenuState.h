#pragma once
#include "Menu.h"
#include "IState.h"
#include "stdafx.h"

class MenuState : public IState
{
public:
	MenuState(System& p_xSystem);
	~MenuState();
	void Enter();
	bool Update(float p_fDeltaTime);
	void Draw();
	void Exit();
	IState* NextState();
	
private:
	int mSelectedState;
	System m_xSystem;
	sf::Sprite SplashScreen;
	Menu mMenu;
};